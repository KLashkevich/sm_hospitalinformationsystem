/**
 * @(#) PatientDAO.java
 */

package InteractionModel;

import ApplicationModel.Patient.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class PatientDAO
{
	public static boolean patientExists( String idCode )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM PatientFolder WHERE idCode = '"+idCode+"'");
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean deletePatientFolder( String idCode )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			ResultSet res = null;

			res = stmt.executeQuery("SELECT testId FROM Test WHERE patientId = '"+idCode+"'");
			while(res.next()){
				int testId =  res.getInt(1);
				stmt.executeUpdate(String.format("DELETE FROM TestSlot WHERE testId = '"+testId+"'"));
			}

			res = stmt.executeQuery("SELECT surgeryId FROM Surgery WHERE patientId = '"+idCode+"'");
			while(res.next()){
				int surgeryId =  res.getInt(1);
				stmt.executeUpdate(String.format("DELETE FROM SurgerySlot WHERE surgeryId = '"+surgeryId+"'"));
				stmt.executeUpdate(String.format("DELETE FROM SurgeryTeamParticipation WHERE surgeryId = '"+surgeryId+"'"));
			}

			res = stmt.executeQuery("SELECT visitId FROM Visit WHERE patientId = '"+idCode+"'");
			while(res.next()){
				int visitId =  res.getInt(1);
				stmt.executeUpdate(String.format("DELETE FROM VisitSlot WHERE visitId = '"+visitId+"'"));
			}

			res = stmt.executeQuery("SELECT therapyId FROM Therapy WHERE patientId = '"+idCode+"'");
			while(res.next()){
				int therapyId =  res.getInt(1);
				stmt.executeUpdate(String.format("DELETE FROM Posology WHERE therapyId = '"+therapyId+"'"));
			}


			stmt.executeUpdate(String.format("DELETE FROM Test WHERE patientId = '"+idCode+"'"));
			stmt.executeUpdate(String.format("DELETE FROM Surgery WHERE patientId = '"+idCode+"'"));
			stmt.executeUpdate(String.format("DELETE FROM Visit WHERE patientId = '"+idCode+"'"));
			stmt.executeUpdate(String.format("DELETE FROM Therapy WHERE patientId = '"+idCode+"'"));

			result = stmt.executeUpdate(String.format("DELETE FROM PatientFolder WHERE idCode = '"+idCode+"'"));
			con.commit();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public static boolean insertPatientFolder( PatientFolder folder )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			Patient patient = folder.getPatient();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String stringDate = df.format(patient.getBirthDate());
			String className = patient.getClass().getSimpleName();
			String stringFirstVisitDate = "NULL";
			if (folder.getFirstVisitDate() != null) {
				stringFirstVisitDate = "'"+df.format(folder.getFirstVisitDate())+"'";
			}
			String caseFollowerId = "";
			if(folder.getCaseFollower() != null){
				caseFollowerId = folder.getCaseFollower().getProfessionalID();
			}

			int insuranceType;

			if(className.equals("NationalInsurancePatient")) {
				insuranceType = 0;
			}
			else {
				insuranceType = 1;
			}

			result = stmt.executeUpdate(String.format("INSERT INTO PatientFolder (idCode, name, surname, birthDate, insuranceType, insuranceCode, insuranceCompany, firstVisitDate, caseFollowerID) " +
							"VALUES ('%s', '%s', '%s', '%s', %s, '%s', '%s', %s, '%s')",
					patient.getIdCode(),
					patient.getName(),
					patient.getSurname(),
					stringDate,
					insuranceType,
					patient.getInsuranceCode(),
					patient.getInsuranceCompany(),
					stringFirstVisitDate,
					caseFollowerId)
					);
			con.commit();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}

	public static List<PatientFolder> getPatients( )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<PatientFolder> output = new ArrayList<PatientFolder>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM PatientFolder");
			int insuranceType;

			while(result.next()){
				PatientFolder folder = new PatientFolder();
				folder.setCatalogueCode(result.getString("catalogueCode"));
				//folder.setCaseFollower(result.getString("catalogueCode"));

				DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
				try {
					folder.setFirstVisitDate(format.parse(result.getString("firstVisitDate")));
				} catch (Exception e) {
					folder.setFirstVisitDate(new GregorianCalendar(1990, 1, 1).getTime());
				}

				insuranceType = result.getByte("insuranceType");

				Patient patient = PatientFactory.getPatient(result.getString("idCode"),insuranceType,result.getString("insuranceCompany"));
				patient.setInsuranceCode(result.getString("insuranceCode"));
				patient.setName(result.getString("name"));
				patient.setSurname(result.getString("surname"));
				try {
					patient.setBirthDate(format.parse(result.getString("birthDate")));
				} catch (Exception e) {
					patient.setBirthDate(new GregorianCalendar(1990, 1, 1).getTime());
				}

				folder.setPatient(patient);
				output.add(folder);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean updateOncologist( String patientID, String professionalID )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();

			result = stmt.executeUpdate(String.format("UPDATE PatientFolder SET caseFollowerID = '%s' WHERE idCode = '%s'",
					professionalID,
					patientID)
			);
			con.commit();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public static boolean updateFirstVisit( String idCode, Date visitDate )
	{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String stringDate = df.format(visitDate);

		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();

			result = stmt.executeUpdate(String.format("UPDATE PatientFolder SET firstVisitDate = '%s' WHERE idCode = '%s'",
					stringDate,
					idCode)
			);
			con.commit();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}

	
	public static boolean updateCatalogueCode( String patientId, String code )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();

			result = stmt.executeUpdate(String.format("UPDATE PatientFolder SET catalogueCode = '%s' WHERE idCode = '%s'",
					code,
					patientId)
			);
			con.commit();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public void getPatientFolderInfo( String idCode )
	{
		
	}
	
}
