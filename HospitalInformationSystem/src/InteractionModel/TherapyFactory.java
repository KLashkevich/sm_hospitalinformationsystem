package InteractionModel;
import ApplicationModel.Therapy.*;

public class TherapyFactory {
    public static Therapy getTherapy (short type) {
        Therapy therapy;
        switch (type) {
            case 1:
                therapy = new HomeTherapy();
                break;
            case 2:
                therapy = new HospitalDayTherapy();
                break;
            case 3:
                therapy = new HospitalOvernightTherapy();
                break;
            default:
                therapy = new HomeTherapy();
                break;
        }
        return therapy;
    }

}
