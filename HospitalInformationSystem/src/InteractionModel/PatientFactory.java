/**
 * @(#) PatientFactory.java
 */

package InteractionModel;

import ApplicationModel.Patient.NationalInsurancePatient;
import ApplicationModel.Patient.Patient;
import ApplicationModel.Patient.PrivateInsurancePatient;

public class PatientFactory
{
	public static Patient getPatient( String idCode, int insuranceType, String insCompany )
	{
		Patient patient;
		if (insuranceType == 1) {
			patient = new PrivateInsurancePatient();
			((PrivateInsurancePatient) patient).setCompanyName(insCompany);
		}
		else {
			patient = new NationalInsurancePatient();
		}
		patient.setIdCode(idCode);

		return patient;
	}
}
