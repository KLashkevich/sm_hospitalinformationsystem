/**
 * @(#) UserDAO.java
 */

package InteractionModel;

import ApplicationModel.User;
import ApplicationModel.UserRole;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class UserDAO
{
	public static boolean insertUser( User user )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeUpdate("INSERT INTO User (username, password, roleID) VALUES ('"
					+ user.getName() +"', '" + user.getPassword() +	"', " + user.getRole().ordinal() + ")");

			con.commit();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public static boolean userExists( String username )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM User WHERE username = '"+username+"'");
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static User getUserWithPassword( String username, String password )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		User user = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM User WHERE username = '"+username+"' AND password = '"+password+"'");

			while(result.next()){
				user = new User();
				user.setName(result.getString("username"));
				user.setPassword(result.getString("password"));
				user.setRole(UserRole.values()[result.getShort("roleID")]);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return user;
	}
	
	
}
