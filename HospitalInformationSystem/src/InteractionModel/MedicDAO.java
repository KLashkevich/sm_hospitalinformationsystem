/**
 * @(#) MedicDAO.java
 */

package InteractionModel;

import ApplicationModel.Medic.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import Utilities.DateUtil;

public class MedicDAO
{
	public static boolean medicExists( String professionalID )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM Oncologist WHERE professionalID = '"+professionalID+"'");
			while(result.next()){
				output= true;
			}
			if (!output) {
				result = stmt.executeQuery(
						"SELECT * FROM Surgeon WHERE professionalID = '"+professionalID+"'");
				while(result.next()){
					output= true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean insertOncologist( Oncologist oncologist )
	{
		String className = oncologist.getClass().getName();

		Map<String, String> careerMap = new HashMap<String, String>();
		careerMap.put("ApplicationModel.Medic.MedicalStudent", "Medical Student");
		careerMap.put("ApplicationModel.Medic.PhysicianAssistant", "Physician Assistant");
		careerMap.put("ApplicationModel.Medic.Fellow", "Fellow");
		careerMap.put("ApplicationModel.Medic.Specialist", "Specialist");

		String careerLevel = careerMap.get(className);

		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeUpdate("INSERT INTO Oncologist (professionalID, name, surname, careerLevel, oncologistTypeId )" +
					" VALUES ('"
					+ oncologist.getProfessionalID() +"', '" + oncologist.getName() +	"', '"
					+ oncologist.getSurname() +"', '" + careerLevel + "', "
					+ oncologist.getType().ordinal() + ")");
			con.commit();

			if (careerLevel.equals("Fellow") || careerLevel.equals("Specialist")){
				Date currentDate = Calendar.getInstance().getTime();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String stringDate;

				String[] startTimes = {"09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30"};
				String[] endTimes = {"09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00"};

				for (int i = 1; i <= 31; i++) {
					currentDate = DateUtil.addDays(currentDate, 1);
					stringDate = df.format(currentDate);
					for (int j = 0; j < 10; j++) {
						stmt.executeUpdate(String.format("INSERT INTO VisitSlot (doctorId, visitDate, startTime, endTime)\n" +
								"VALUES ('%s', '%s', '%s', '%s')", oncologist.getProfessionalID(), stringDate, startTimes[j], endTimes[j]));
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public static List<Oncologist> getOncologists(String careerLevel)
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<Oncologist> output = new ArrayList<Oncologist>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			String query = "SELECT * FROM Oncologist";
			if (careerLevel != null){
				query += String.format(" WHERE careerLevel = '%s'", careerLevel);
			}

			result = stmt.executeQuery(
					query);

			while(result.next()){
				Oncologist oncologist = MedicFactory.getOncologist(result.getString("professionalID"),result.getString("careerLevel"));
				oncologist.setType(OncologistType.values()[result.getShort("oncologistTypeId")]);
				oncologist.setName(result.getString("name"));
				oncologist.setSurname(result.getString("surname"));
				output.add(oncologist);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean insertSurgeon( Surgeon surgeon )
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeUpdate("INSERT INTO Surgeon (professionalID, name, surname, surgeonTypeId )" +
					" VALUES ('"
					+ surgeon.getProfessionalID() +"', '" + surgeon.getName() +	"', '"
					+ surgeon.getSurname() + "', "
					+ surgeon.getType().ordinal() + ")");
			con.commit();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public static List<Surgeon> getSurgeons( )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<Surgeon> output = new ArrayList<Surgeon>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM Surgeon");

			while(result.next()){
				Surgeon surgeon = MedicFactory.getSurgeon(result.getString("professionalID"));
				surgeon.setType(SurgeonType.values()[result.getShort("surgeonTypeId")]);
				surgeon.setName(result.getString("name"));
				surgeon.setSurname(result.getString("surname"));
				output.add(surgeon);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static List<Oncologist> getAvailableDoctors()
	{
		List<Oncologist> result = getOncologists("Specialist");
		result.addAll(getOncologists("Fellow"));
		return result;
	}
	
	public static List<Surgeon> getAvailableSurgeons( SurgeonType surgeonType, Date date )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<Surgeon> output = new ArrayList<Surgeon>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String stringDate = df.format(date);

		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(String.format("SELECT * FROM Surgeon AS s " +
					"LEFT JOIN Surgery sr ON s.professionalID = sr.surgeonId " +
					"LEFT JOIN SurgerySlot ss ON ss.surgeryId = sr.surgeryId AND ss.surgeryDate = '%s' " +
					"WHERE s.surgeonTypeId = %s " +
					"AND ss.slotId IS NULL", stringDate, surgeonType.ordinal()));

			while(result.next()){
				Surgeon surgeon = MedicFactory.getSurgeon(result.getString("professionalID"));
				surgeon.setType(SurgeonType.values()[result.getShort("surgeonTypeId")]);
				surgeon.setName(result.getString("name"));
				surgeon.setSurname(result.getString("surname"));
				output.add(surgeon);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean createNewSurgery( int surgerySlotId, String professionalID, String patientId )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet selectResult = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();

			result = stmt.executeUpdate(String.format("INSERT INTO Surgery (slotId, surgeonId, patientId) VALUES (%s, '%s', '%s')",
					surgerySlotId,
					professionalID,
					patientId
			));
			con.commit();

			selectResult = stmt.executeQuery(String.format("SELECT * FROM Surgery WHERE patientId = '%s' AND slotId = %s", patientId, surgerySlotId));
			while(selectResult.next()){
				int surgeryId = selectResult.getInt("surgeryId");
				result = stmt.executeUpdate(String.format("UPDATE SurgerySlot SET surgeryId = %s WHERE slotId = %s",
						surgeryId,
						surgerySlotId
				));
				con.commit();
			}
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public void getAvailableTeamMembers( String patientId )
	{
		
	}
	
	public boolean teamMemberExists( String surgeryId, String professionalID )
	{
		return false;
	}
	
	
}
