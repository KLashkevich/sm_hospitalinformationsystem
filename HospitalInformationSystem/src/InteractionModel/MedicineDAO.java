/**
 * @(#) MedicineDAO.java
 */

package InteractionModel;

import ApplicationModel.Medicine.Medicine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MedicineDAO
{
	public static boolean medicineExists( String code )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM Medicine WHERE medicineId = '"+code+"'");
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean insertMedicine( Medicine med)
	{
		Connection con = null;
		Statement stmt = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeUpdate("INSERT INTO Medicine (medicineId, medicineName, pharmCompany) VALUES ('"
					+ med.getCode() + "', '" + med.getName() +	"', '" + med.getPharmaceuticalCompany() + "')");

			con.commit();
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public static List<Medicine> getMedicines( )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<Medicine> output = new ArrayList<Medicine>();
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(
					"SELECT * FROM Medicine");

			while(result.next()){
				Medicine medicine = new Medicine();
				medicine.setName(result.getString("medicineName"));
				medicine.setCode(result.getString("medicineId"));
				medicine.setCompany(result.getString("pharmCompany"));

				output.add(medicine);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	
}
