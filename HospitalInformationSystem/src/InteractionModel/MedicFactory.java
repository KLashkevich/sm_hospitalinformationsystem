/**
 * @(#) MedicFactory.java
 */

package InteractionModel;

import ApplicationModel.Medic.*;

public class MedicFactory
{
	public static Oncologist getOncologist(String professionalID, String careerLevel )
	{
		Oncologist oncologist;
		switch (careerLevel){
			case "Medical Student":
				oncologist = new MedicalStudent();
				break;
			case "Physician Assistant":
				oncologist = new PhysicianAssistant();
				break;
			case "Fellow":
				oncologist = new Fellow();
				break;
			case "Specialist":
				oncologist = new Specialist();
				break;
			default:
				oncologist = new MedicalStudent();
				break;

		}
		oncologist.setProfessionalID(professionalID);
		return oncologist;
	}
	
	public static Surgeon getSurgeon( String professionalID )
	{
		Surgeon surgeon = new Surgeon();
		surgeon.setProfessionalID(professionalID);
		return surgeon;
	}
	
	
}
