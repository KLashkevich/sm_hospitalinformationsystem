/**
 * @(#) Controller.java
 */

package InteractionModel;

import java.util.Date;
import java.util.List;

import ApplicationModel.*;
import ApplicationModel.Medic.*;
import ApplicationModel.Medicine.Medicine;
import ApplicationModel.Patient.Patient;
import ApplicationModel.Patient.PatientFolder;
import ApplicationModel.Surgery.SurgeryCalendar;
import ApplicationModel.Surgery.SurgeryDailySlot;
import ApplicationModel.Tests.BloodTestsCalendar;
import ApplicationModel.Tests.ImagingTestType;
import ApplicationModel.Tests.ImagingTestsCalendar;
import ApplicationModel.Tests.TestDailySlot;
import ApplicationModel.Therapy.Therapy;
import ApplicationModel.Therapy.TherapyCalendar;
import ApplicationModel.Visit.DailyVisitSlot;
import ApplicationModel.Visit.VisitCalendar;

public class Controller
{
	public static boolean registerUser( String username, String password, UserRole role ) throws Exception {
		if (UserDAO.userExists(username)){
			throw new Exception("ERROR: User with such username is already registered!");
		}

		User user = new User();
		user.setName(username);
		user.setPassword(password);
		user.setRole(role);

		return UserDAO.insertUser(user);
	}
	
	public static User loginUser( String username, String password ) throws Exception {
		if (!UserDAO.userExists(username)){
			throw new Exception("ERROR: User does not exist in the database");
		}
		return UserDAO.getUserWithPassword(username, password);
	}
	
	public static boolean addOncologist( String name, String surname, OncologistType type, String careerLevel, String professionalID ) throws Exception {
		if (MedicDAO.medicExists(professionalID)) {
			throw new Exception("ERROR: The medic with such Professional ID already exists");
		}
		Oncologist oncologist = MedicFactory.getOncologist(professionalID, careerLevel);
		oncologist.setName(name);
		oncologist.setSurname(surname);
		oncologist.setType(type);
		return MedicDAO.insertOncologist(oncologist);
	}
	
	public static List<Oncologist> getOncologists(String careerLevel)
	{
		return MedicDAO.getOncologists(careerLevel);
	}
	
	public static boolean addSurgeon( String name, String surname, SurgeonType type, String professionalID ) throws Exception {
		if (MedicDAO.medicExists(professionalID)) {
			throw new Exception("ERROR: The medic with such Professional ID already exists");
		}

		Surgeon surgeon = MedicFactory.getSurgeon(professionalID);
		surgeon.setName(name);
		surgeon.setSurname(surname);
		surgeon.setType(type);

		return MedicDAO.insertSurgeon(surgeon);
	}
	
	public static List<Surgeon> getSurgeons( )
	{
		return MedicDAO.getSurgeons();
	}

	public static boolean createPatientFolder(String name, String surname, String idCode, Date birthDate, int insuranceType, String insCode, String insCompany ) throws Exception {
		if (PatientDAO.patientExists(idCode)) {
			throw new Exception("ERROR: The patient with such ID code already exists");
		}

		Patient patient = PatientFactory.getPatient(idCode, insuranceType, insCompany);
		if (insuranceType == 0) { //if national
			patient.setInsuranceCode(surname+idCode);
		}
		else { //if private
			patient.setInsuranceCode(insCode);
		}

		patient.setName(name);
		patient.setSurname(surname);
		patient.setBirthDate(birthDate);

		PatientFolder folder = new PatientFolder();
		folder.setPatient(patient);
		//folder.setCaseFollower();
		//folder.setFirstVisitDate();

		return PatientDAO.insertPatientFolder(folder);
	}

	public static List<PatientFolder> getPatients()
	{
		return PatientDAO.getPatients();
	}

	public static boolean assignOncologist( String patientID, String professionalID )
	{
		return PatientDAO.updateOncologist(patientID, professionalID);
	}
	
	public static boolean deletePatientFolder( String idCode )
	{
		return PatientDAO.deletePatientFolder(idCode);
	}
	
	public static boolean setFirstVisitDate( String idCode, Date visitDate )
	{
		return PatientDAO.updateFirstVisit(idCode, visitDate);
	}
	
	public static boolean setCatalogueCode( String patientId, String code )throws Exception
	{
		if (!PatientDAO.patientExists(patientId)) {
			throw new Exception("ERROR: No patient exists with such ID code");
		}
		return PatientDAO.updateCatalogueCode(patientId, code);
	}
	
	public static  List<Oncologist> getAvailableDoctors()
	{
		return MedicDAO.getAvailableDoctors();
	}
	
	public static List<DailyVisitSlot> getDateSlots(String professionalID )
	{
		return VisitCalendar.getDateSlots(professionalID);
	}
	
	public static boolean bookVisitDateAndTime( int timeSlotId, String patientId, String followerProfessionalID )
	{
		return VisitCalendar.createNewVisit(timeSlotId, patientId, followerProfessionalID);
	}
	

	public static List<TestDailySlot> getBlTestsDaySlots(Date date )
	{
		return BloodTestsCalendar.getBlTestsDaySlots(date);
	}
	
	public static boolean bookBlTestDateTime( String patientId, short timeSlotId )
	{
		return BloodTestsCalendar.createNewBloodTest(patientId, timeSlotId);
	}
	
	public static List<TestDailySlot> getImgTestsDaySlots( Date date )
	{
		return ImagingTestsCalendar.getImgTestsDaySlots(date);
	}

	public static List<SurgeryDailySlot> getSurgeryDaySlots(SurgeonType type, Date date)
	{
		return SurgeryCalendar.getSurgeryDaySlots(type, date);
	}
	
	public static boolean bookImgTestDateTime(String patientId, short timeSlotId, ImagingTestType imgTestType)
	{
		return ImagingTestsCalendar.createNewImgTest(patientId, timeSlotId, imgTestType);
	}
	

	public String getTestReport( String testId )
	{
		return null;
	}
	
	public static List<Surgeon> getAvailableSurgeons( SurgeonType surgeonType, Date date )
	{
		return MedicDAO.getAvailableSurgeons(surgeonType, date);
	}
	
	public static boolean bookSurgeryDate( int surgerySlotId, String professionalID, String patientId)
	{
		return MedicDAO.createNewSurgery(surgerySlotId, professionalID, patientId);
	}
	
	public static boolean addMedicine( String code, String name, String pharmaceuticalCompany ) throws Exception
	{
		if (MedicineDAO.medicineExists(code)) {
			throw new Exception("ERROR: Such medicine already exists");
		}
		//create medicine
		Medicine med = new Medicine();
		med.setCode(code);
		med.setCompany(pharmaceuticalCompany);
		med.setName(name);

		return MedicineDAO.insertMedicine(med);
	}
	
	public static List<Medicine> getMedicines( )
	{
		return MedicineDAO.getMedicines();
	}
	
	public static boolean checkTherapyOverlapping( String patientId, Date startDate, Date endDate )
	{
		return TherapyCalendar.isTherapyOverlapping(patientId, startDate, endDate);
	}

	public static boolean isAvailableTherapyPeriod (short therapyType, Date startDate, Date endDate)
	{
		if (therapyType == 1) //for home therapies there is no need of booking the date slots
		{
			return true;
		}
		return TherapyCalendar.isAvailablePeriod(therapyType, startDate, endDate);
	}

	public static boolean createTherapy (short therapyType, Therapy therapy)
	{
		return TherapyCalendar.createTherapy(therapyType, therapy);
	}

	public void getPatientTests( String idCode )
	{

	}

	public void getPatientVisits( String idCode )
	{

	}

	public void updateVisitReport( String visitId, String report )
	{

	}
	
	public void addTreatmentMedicine( String medCode, Date startDate, Date endDate )
	{
		
	}
	
	public void viewPatientFolder( String idCode )
	{
		
	}
	
	public void getUpcomingSurgeries( String professionalID )
	{
		
	}
	
	public void getAvailableTeamMembers( String surgeryId )
	{
		
	}
	
	public void addTeamMember( String surgeryId, String professionalID )
	{
		
	}
	
	
}
