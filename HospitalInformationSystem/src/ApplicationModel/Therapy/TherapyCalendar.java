/**
 * @(#) TherapyCalendar.java
 */

package ApplicationModel.Therapy;

import ApplicationModel.Medicine.Medicine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TherapyCalendar
{
	//private TherapyDailySlot dates;
	
	public static boolean isTherapyOverlapping( String patientId, Date startDate, Date endDate )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String stringStartDate = df.format(startDate);
		String stringEndDate = df.format(endDate);

		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			result = stmt.executeQuery(String.format("SELECT * FROM Therapy " +
					"WHERE " +
					"patientId = '%s' " +
					"AND " +
					"((startDate BETWEEN '%s' AND '%s') " +
					"OR (endDate BETWEEN '%s' AND '%s'))", patientId, stringStartDate, stringEndDate, stringStartDate, stringEndDate));
			while(result.next()){
				output= true;
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean isAvailablePeriod( short therapyType, Date startDate, Date endDate )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		boolean output = false;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String stringStartDate = df.format(startDate);
		String stringEndDate = df.format(endDate);

		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();

			if (therapyType == 2) { //single day hospital therapy
				result = stmt.executeQuery(String.format("SELECT * FROM TherapySlot " +
						"WHERE TherapyDate = '%s' " +
						"AND availableTotalPlaces > 0", stringStartDate));
				while(result.next()){
					output= true;
				}
			}
			else if (therapyType == 3) { //overnight hospitalization therapy
				long days = TimeUnit.DAYS.convert(endDate.getTime() - startDate.getTime(), TimeUnit.MILLISECONDS) + 1;
				long slotsCnt = 0;
				result = stmt.executeQuery(String.format("SELECT TherapyDate FROM TherapySlot " +
						"WHERE TherapyDate BETWEEN '%s' AND '%s' " +
						"AND availableTotalPlaces > 0 " +
						"GROUP BY TherapyDate", stringStartDate, stringEndDate));
				while(result.next()){
					slotsCnt ++;
				}
				if (slotsCnt == days) {
					output = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean createTherapy( short therapyType, Therapy therapy )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet selectResult = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();

			String mainQuery = String.format("INSERT INTO Therapy (startDate, endDate, therapyTypeId, patientId)\n" +
							"VALUES ('%s', '%s', %s, '%s')",
					df.format(therapy.getStartDate()),
					df.format(therapy.getEndDate()),
					therapyType,
					therapy.getPatientId()
			);

			result = stmt.executeUpdate(mainQuery);
			con.commit();

			if(result != 0){
				selectResult = stmt.executeQuery(String.format("SELECT TOP 1 * FROM Therapy WHERE patientId = '%s' ORDER BY therapyId DESC",
						therapy.getPatientId()));
				short therapyId;
				while(selectResult.next()){
					therapyId = selectResult.getShort("therapyId");
					for (Map.Entry<Medicine, Posology> entry : therapy.getPosologies().entrySet()) {
						result = stmt.executeUpdate(String.format("INSERT INTO Posology (startDate, endDate, unitsPerDay, medicineId, therapyId) " +
										"VALUES ('%s', '%s', %s, '%s', %s)",
								df.format(entry.getValue().getStartDate()),
								df.format(entry.getValue().getEndDate()),
								entry.getValue().getUnitsPerDay(),
								entry.getKey().getCode(),
								therapyId));
						con.commit();
					}
				}

				if (therapyType == 2) { //Day Hospital Therapy
					result = stmt.executeUpdate(String.format("UPDATE TherapySlot SET availableTotalPlaces = availableTotalPlaces - 1 " +
									"WHERE therapyDate = '%s'",
							df.format(therapy.getStartDate())));

					con.commit();
				}
				if (therapyType == 3) { //Day Hospital Therapy
					result = stmt.executeUpdate(String.format("UPDATE TherapySlot SET availableTotalPlaces = availableTotalPlaces - 1, availableOverNightPlaces = availableOverNightPlaces - 1 " +
									"WHERE therapyDate BETWEEN '%s' AND '%s'",
							df.format(therapy.getStartDate()),
							df.format(therapy.getEndDate())
					));
					con.commit();
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	
}
