package ApplicationModel.Therapy;

import java.util.Date;

public class Posology {

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public short getUnitsPerDay() {
        return unitsPerDay;
    }

    public void setUnitsPerDay(short unitsPerDay) {
        this.unitsPerDay = unitsPerDay;
    }

    private Date startDate;

    private Date endDate;

    private short unitsPerDay;

}
