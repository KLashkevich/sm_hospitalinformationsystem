/**
 * @(#) Therapy.java
 */

package ApplicationModel.Therapy;

import ApplicationModel.Medic.Medic;
import ApplicationModel.Patient.PatientFolder;
import ApplicationModel.Medicine.Medicine;

import java.util.Date;
import java.util.Map;

public abstract class Therapy
{
	//private Medicine medicine;
	
	//private PatientFolder folder;

	private String patientId;

	private Date startDate;
	
	private Date endDate;

	private Map<Medicine, Posology> posologies;

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public boolean containsMedicine( String medCode )
	{
		boolean result = false;
		for (Medicine medicine : posologies.keySet()) {
			if (medicine.getCode().equals(medCode)){
				result = true;
				break;
			}
		}

		return result;
	}
	
	public void addMedicine( Medicine medicine, Posology posology )
	{
		posologies.put(medicine, posology);
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Map<Medicine, Posology> getPosologies() {
		return posologies;
	}

	public void setPosologies(Map<Medicine, Posology> posologies) {
		this.posologies = posologies;
	}
}
