/**
 * @(#) TherapyDailySlot.java
 */

package ApplicationModel.Therapy;

import java.util.Date;
import java.util.List;

public class TherapyDailySlot
{
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getAvailableTotalPlaces() {
		return availableTotalPlaces;
	}

	public void setAvailableTotalPlaces(int availableTotalPlaces) {
		this.availableTotalPlaces = availableTotalPlaces;
	}

	public int getAvailableOverNightPlaces() {
		return availableOverNightPlaces;
	}

	public void setAvailableOverNightPlaces(int availableOverNightPlaces) {
		this.availableOverNightPlaces = availableOverNightPlaces;
	}

	private Date date;
	
	private int availableTotalPlaces;
	
	private int availableOverNightPlaces;
	
	//private List<HospitalDayTherapy> dayTherapies;
	
	//private List<HospitalOvernightTherapy> overnightTherapies;
	
}
