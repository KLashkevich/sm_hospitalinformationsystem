package ApplicationModel;

public enum UserRole {
    AdminOfficer,
    Receptionist,
    Doctor,
    Surgeon
}
