package ApplicationModel.Tests;

public enum ImagingTestType {
    XRay,
    PET,
    TC,
    MRI
}
