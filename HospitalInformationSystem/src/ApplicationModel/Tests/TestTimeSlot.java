/**
 * @(#) TestTimeSlot.java
 */

package ApplicationModel.Tests;

public class TestTimeSlot
{
	private String startTime;
	
	private String endTime;
	
	private Test test;
	
	private short timeSlotId;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public short getTimeSlotId() {
		return timeSlotId;
	}

	public void setTimeSlotId(short timeSlotId) {
		this.timeSlotId = timeSlotId;
	}
}
