/**
 * @(#) BloodTestsCalendar.java
 */

package ApplicationModel.Tests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BloodTestsCalendar
{
	private TestDailySlot bloodSlots;
	
	public static List<TestDailySlot> getBlTestsDaySlots(Date date )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<TestDailySlot> output = new ArrayList<TestDailySlot>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			String query;

			if (date != null) {
				String stringDate = df.format(date);
				query = "SELECT * FROM TestSlot  WHERE testDate  > CURRENT_DATE " +
						"AND testId IS NULL " +
						"AND typeId = 0 " +
						"AND testDate = '" + stringDate + "' " +
						"ORDER BY testDate, startTime";
			}
			else {
				query = "SELECT * FROM TestSlot  WHERE testDate  > CURRENT_DATE " +
						"AND testId IS NULL " +
						"AND typeId = 0 " +
						"ORDER BY testDate, startTime";
			}

			result = stmt.executeQuery(
					query);

			TestDailySlot currentDailySlot = null;
			List<TestTimeSlot> currentTimeSlots = null;
			String currentDate = "";

			while(result.next()){
				String dateStr = result.getString("testDate");
				if (!dateStr.equals(currentDate)) {
					currentDate = dateStr;
					if (currentDailySlot != null) {
						currentDailySlot.setSlots(currentTimeSlots);
						output.add(currentDailySlot);
					}
					currentDailySlot = new TestDailySlot();
					currentDailySlot.setDate(result.getDate("testDate"));
					currentTimeSlots = new ArrayList<TestTimeSlot>();
				}
				TestTimeSlot ts = new TestTimeSlot();
				ts.setTimeSlotId(result.getShort("slotId"));
				ts.setStartTime(result.getString("startTime"));
				ts.setEndTime(result.getString("endTime"));
				currentTimeSlots.add(ts);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean createNewBloodTest( String patientId, short timeSlotId )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet selectResult = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			//BloodTest test = new BloodTest();

			result = stmt.executeUpdate(String.format("INSERT INTO Test (typeId, patientId, slotId) VALUES (0, '%s', %s)",
					patientId,
					timeSlotId
			));
			con.commit();

			selectResult = stmt.executeQuery(String.format("SELECT * FROM Test WHERE patientId = '%s' AND slotId = %s", patientId, timeSlotId));
			while(selectResult.next()){
				short testId = selectResult.getShort("testId");
				result = stmt.executeUpdate(String.format("UPDATE TestSlot SET testId = %s WHERE slotId = %s",
						testId,
						timeSlotId
				));
				con.commit();
			}
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	
}
