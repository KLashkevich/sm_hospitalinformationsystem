/**
 * @(#) ImagingTest.java
 */

package ApplicationModel.Tests;

import java.util.List;

public class ImagingTest extends Test
{
	private ImagingTestType type;
	
	private List<byte[]> images; //unused

	public ImagingTestType getType() {
		return type;
	}

	public void setType(ImagingTestType type) {
		this.type = type;
	}
}
