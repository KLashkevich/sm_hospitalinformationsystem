/**
 * @(#) TestDailySlot.java
 */

package ApplicationModel.Tests;

import java.util.Date;
import java.util.List;

public class TestDailySlot
{
	private Date date;
	private List<TestTimeSlot> slots;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<TestTimeSlot> getSlots() {
		return slots;
	}

	public void setSlots(List<TestTimeSlot> slots) {
		this.slots = slots;
	}
}
