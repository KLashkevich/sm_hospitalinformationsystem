/**
 * @(#) Test.java
 */

package ApplicationModel.Tests;

import ApplicationModel.Patient.PatientFolder;

public abstract class Test
{
	private String testId;

	private PatientFolder folder;

	private TestTimeSlot slot;

	private String report; //unused

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	public PatientFolder getFolder() {
		return folder;
	}

	public void setFolder(PatientFolder folder) {
		this.folder = folder;
	}

	public TestTimeSlot getSlot() {
		return slot;
	}

	public void setSlot(TestTimeSlot slot) {
		this.slot = slot;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}
}
