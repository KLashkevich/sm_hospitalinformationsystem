/**
 * @(#) ImagingTestsCalendar.java
 */

package ApplicationModel.Tests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ImagingTestsCalendar
{
	private ImagingTest tests;
	
	private TestDailySlot imagingSlots;
	
	public static List<TestDailySlot> getImgTestsDaySlots(Date date )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<TestDailySlot> output = new ArrayList<TestDailySlot>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			String query;

			if (date != null) {
				String stringDate = df.format(date);
				query = "SELECT * FROM TestSlot  WHERE testDate  > CURRENT_DATE " +
						"AND testId IS NULL " +
						"AND typeId = 1 " +
						"AND testDate = '" + stringDate + "' " +
						"ORDER BY testDate, startTime";
			}
			else {
				query = "SELECT * FROM TestSlot  WHERE testDate  > CURRENT_DATE " +
						"AND testId IS NULL " +
						"AND typeId = 1 " +
						"ORDER BY testDate, startTime";
			}

			result = stmt.executeQuery(
					query);

			TestDailySlot currentDailySlot = null;
			List<TestTimeSlot> currentTimeSlots = null;
			String currentDate = "";

			while(result.next()){
				String dateStr = result.getString("testDate");
				if (!dateStr.equals(currentDate)) {
					currentDate = dateStr;
					if (currentDailySlot != null) {
						currentDailySlot.setSlots(currentTimeSlots);
						output.add(currentDailySlot);
					}
					currentDailySlot = new TestDailySlot();
					currentDailySlot.setDate(result.getDate("testDate"));
					currentTimeSlots = new ArrayList<TestTimeSlot>();
				}
				TestTimeSlot ts = new TestTimeSlot();
				ts.setTimeSlotId(result.getShort("slotId"));
				ts.setStartTime(result.getString("startTime"));
				ts.setEndTime(result.getString("endTime"));
				currentTimeSlots.add(ts);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean createNewImgTest( String patientId, short timeSlotId, ImagingTestType imgTestType )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet selectResult = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			ImagingTest test = new ImagingTest();
			test.setType(imgTestType);

			result = stmt.executeUpdate(String.format("INSERT INTO Test (typeId, imagingTypeId, patientId, slotId) VALUES (1, %s, '%s', %s)",
					imgTestType.ordinal(),
					patientId,
					timeSlotId
			));
			con.commit();

			selectResult = stmt.executeQuery(String.format("SELECT * FROM Test WHERE patientId = '%s' AND slotId = %s", patientId, timeSlotId));
			while(selectResult.next()){
				short testId = selectResult.getShort("testId");
				result = stmt.executeUpdate(String.format("UPDATE TestSlot SET testId = %s WHERE slotId = %s",
						testId,
						timeSlotId
				));
				con.commit();
			}
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
}
