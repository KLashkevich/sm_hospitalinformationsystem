/**
 * @(#) PatientFolder.java
 */

package ApplicationModel.Patient;

import ApplicationModel.Medic.Specialist;
import ApplicationModel.Therapy.Therapy;
import ApplicationModel.Tests.Test;
import ApplicationModel.Visit.Visit;
import ApplicationModel.Surgery.Surgery;

import java.util.Date;
import java.util.List;

public class PatientFolder
{
	private Patient patientData;
	private String anamnesis;
	private Date firstVisitDate;
	private Specialist caseFollower;
	private String catalogueCode;

	private List<Test> tests;
	private List<Therapy> therapies;
	private List<Surgery> surgeries;
	private List<Visit> visits;

	public  Patient getPatient() {
		return this.patientData;
	}

	public void setPatient( Patient patient ) {
		this.patientData = patient;
	}

	public String getAnamnesis() {
		return this.anamnesis;
	}

	public void setAnamnesis(String anamnesis) {
		this.anamnesis = anamnesis;
	}

	public Date getFirstVisitDate() {
		return firstVisitDate;
	}

	public void setFirstVisitDate(Date firstVisitDate) {
		this.firstVisitDate = firstVisitDate;
	}

	public Specialist getCaseFollower() {
		return caseFollower;
	}

	public void setCaseFollower(Specialist caseFollower) {
		this.caseFollower = caseFollower;
	}

	public String getCatalogueCode() {
		return catalogueCode;
	}

	public void setCatalogueCode(String catalogueCode) {
		this.catalogueCode = catalogueCode;
	}

	public List<Test> getTests() {
		return tests;
	}

	public void setTests(List<Test> tests) {
		this.tests = tests;
	}

	public List<Therapy> getTherapies() {
		return therapies;
	}

	public void setTherapies(List<Therapy> therapies) {
		this.therapies = therapies;
	}

	public List<Surgery> getSurgeries() {
		return surgeries;
	}

	public void setSurgeries(List<Surgery> surgeries) {
		this.surgeries = surgeries;
	}

	public List<Visit> getVisits() {
		return visits;
	}

	public void setVisits(List<Visit> visits) {
		this.visits = visits;
	}
}
