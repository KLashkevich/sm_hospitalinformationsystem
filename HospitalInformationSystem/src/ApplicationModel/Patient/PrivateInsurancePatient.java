/**
 * @(#) PrivateInsurancePatient.java
 */

package ApplicationModel.Patient;

public class PrivateInsurancePatient extends Patient
{
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getInsuranceCompany() {
		return this.companyName;
	}

	private String companyName;
}
