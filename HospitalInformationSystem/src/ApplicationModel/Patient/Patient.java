/**
 * @(#) Patient.java
 */

package ApplicationModel.Patient;

import ApplicationModel.HospitalInformationSystem;

import java.util.Date;

public abstract class Patient
{
	private HospitalInformationSystem infSys;

	private String idCode;
	private String name;
	private String surname;
	private Date birthDate;
	private String insuranceCode;

	public String getIdCode() {
		return idCode;
	}

	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getInsuranceCode() {
		return insuranceCode;
	}

	public void setInsuranceCode(String insuranceCode) {
		this.insuranceCode = insuranceCode;
	}

	public abstract String getInsuranceCompany();
}
