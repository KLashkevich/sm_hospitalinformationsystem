/**
 * @(#) User.java
 */

package ApplicationModel;

public class User
{
	private String username;
	
	private String password;
	
	private UserRole role;

	public String getName()
	{
		return this.username;
	}

	public void setName( String username )
	{
		this.username = username;
	}
	
	public void setPassword( String pass )
	{
		this.password = pass;
	}

	public String getPassword()
	{
		return this.password;
	}
	
	public void setRole( UserRole role )
	{
		this.role = role;
	}

	public UserRole getRole()
	{
		return this.role;
	}
}
