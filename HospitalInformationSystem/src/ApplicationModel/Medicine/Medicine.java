/**
 * @(#) Medicine.java
 */

package ApplicationModel.Medicine;

import ApplicationModel.Therapy.Posology;
import ApplicationModel.Therapy.Therapy;
import ApplicationModel.HospitalInformationSystem;

import java.util.Map;

public class Medicine
{
	private HospitalInformationSystem infSys;
	
	private String name;
	private String code;
	private String pharmaceuticalCompany;
	private Map <Therapy, Posology> posologies;
	
	public void setCode( String code )
	{
		this.code = code;
	}
	
	public void setName( String name )
	{
		this.name = name;
	}
	
	public void setCompany( String pharmaceuticalCompany )
	{
		this.pharmaceuticalCompany = pharmaceuticalCompany;
	}

	public String getName()
	{
		return this.name;
	}

	public String getCode()
	{
		return this.code;
	}

	public String getPharmaceuticalCompany()
	{
		return this.pharmaceuticalCompany;
	}

	public Map<Therapy, Posology> getPosologies() {
		return posologies;
	}

	public void setPosologies(Map<Therapy, Posology> posologies) {
		this.posologies = posologies;
	}
	
	
}
