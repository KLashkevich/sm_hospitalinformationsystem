/**
 * @(#) SurgeryDailySlot.java
 */

package ApplicationModel.Surgery;

import ApplicationModel.Medic.SurgeonType;

import java.util.Date;

public class SurgeryDailySlot
{
	//private Surgery surgeries;

	private Date date;

	private SurgeonType slotType;

	private int slotId;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public SurgeonType getSlotType() {
		return slotType;
	}

	public void setSlotType(SurgeonType slotType) {
		this.slotType = slotType;
	}

	public int getSlotId() {
		return slotId;
	}

	public void setSlotId(int slotId) {
		this.slotId = slotId;
	}
}
