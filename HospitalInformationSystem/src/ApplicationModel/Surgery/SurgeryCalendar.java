/**
 * @(#) SurgeryCalendar.java
 */

package ApplicationModel.Surgery;

import ApplicationModel.Medic.SurgeonType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SurgeryCalendar
{
	private SurgeryDailySlot slots;

	public static List<SurgeryDailySlot> getSurgeryDaySlots(SurgeonType type, Date date) {
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<SurgeryDailySlot> output = new ArrayList<SurgeryDailySlot>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			String query;

			if (date != null) {
				String stringDate = df.format(date);
				query = "SELECT * FROM SurgerySlot WHERE surgeryDate  > CURRENT_DATE " +
						"AND surgeryId IS NULL " +
						"AND surgeryDate = '" + stringDate + "' " +
						"AND typeId = " + type.ordinal() + " " +
						"ORDER BY surgeryDate, typeId";
			}
			else {
				query = "SELECT * FROM SurgerySlot  WHERE surgeryDate  > CURRENT_DATE " +
						"AND surgeryId IS NULL " +
						"AND typeId = " + type.ordinal() + " " +
						"ORDER BY surgeryDate, typeId";
			}

			result = stmt.executeQuery(
					query);

			while(result.next()){
				//String dateStr = result.getString("testDate");
				SurgeryDailySlot slot = new SurgeryDailySlot();
				slot.setDate(result.getDate("surgeryDate"));
				slot.setSlotId(result.getInt("slotId"));
				slot.setSlotType(SurgeonType.values()[result.getInt("typeId")]);
				output.add(slot);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
}
