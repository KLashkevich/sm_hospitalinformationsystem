package ApplicationModel.Medic;

public enum OncologistType {
    Medical,
    Radiation,
    Surgical,
    Gynecologic,
    Pediatric,
    Hematologist
}
