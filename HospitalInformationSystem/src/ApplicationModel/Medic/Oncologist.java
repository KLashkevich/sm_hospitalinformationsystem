/**
 * @(#) Oncologist.java
 */

package ApplicationModel.Medic;

import ApplicationModel.Surgery.Surgery;

public abstract class Oncologist extends Medic
{
	private OncologistType oncologistType;
	
	private Surgery surgery;

	
	public void setType( OncologistType type )
	{
		this.oncologistType = type;
	}

	public OncologistType getType()
	{
		return this.oncologistType;
	}
}
