/**
 * @(#) Medic.java
 */

package ApplicationModel.Medic;

import ApplicationModel.HospitalInformationSystem;

public abstract class Medic
{
	private String name;
	
	private String surname;

	private String professionalID;
	
	private HospitalInformationSystem infSys;

	public void setName( String name )
	{
		this.name = name;
	}

	public String getName()
	{
		return this.name;
	}

	public void setSurname( String surname)
	{
		this.surname = surname;
	}

	public String getSurname()
	{
		return this.surname;
	}

	public void setProfessionalID(String professionalID)
	{
		this.professionalID = professionalID;
	}

	public String getProfessionalID()
	{
		return this.professionalID;
	}
	
}
