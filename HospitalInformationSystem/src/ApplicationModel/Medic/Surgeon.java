/**
 * @(#) Surgeon.java
 */

package ApplicationModel.Medic;

import ApplicationModel.Surgery.Surgery;

public class Surgeon extends Medic
{
	private SurgeonType surgeonType;
	
	private Surgery surgery;

	public void setType( SurgeonType type )
	{
		this.surgeonType = type;
	}

	public SurgeonType getType()
	{
		return this.surgeonType;
	}
	
	
}
