package ApplicationModel.Medic;

public enum SurgeonType {
    General,
    Pediatric,
    Cardiothoracic,
    Neurosurgery,
    OralAndMaxillofacial,
    Urology
}
