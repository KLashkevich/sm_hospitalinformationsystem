/**
 * @(#) Visit.java
 */

package ApplicationModel.Visit;

import ApplicationModel.Medic.Doctor;
import ApplicationModel.Patient.PatientFolder;
import ApplicationModel.Therapy.Therapy;
import ApplicationModel.Tests.Test;
import ApplicationModel.Surgery.Surgery;

public class Visit
{
	public PatientFolder getFolder() {
		return folder;
	}

	public void setFolder(PatientFolder folder) {
		this.folder = folder;
	}

	public Doctor getFollower() {
		return follower;
	}

	public void setFollower(Doctor follower) {
		this.follower = follower;
	}

	public Doctor getHost() {
		return host;
	}

	public void setHost(Doctor host) {
		this.host = host;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	public VisitTimeSlot getSlot() {
		return slot;
	}

	public void setSlot(VisitTimeSlot slot) {
		this.slot = slot;
	}

	public String getVisitId() {
		return visitId;
	}

	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}

	private PatientFolder folder;
	
	private Doctor follower;
	
	private Doctor host;
	
	private String report;
	
	private VisitTimeSlot slot;
	
	private Therapy prescribedTherapies;
	
	private Surgery prescribedSurgeries;
	
	private Test prescribedTests;
	
	private String visitId;
	
	
}
