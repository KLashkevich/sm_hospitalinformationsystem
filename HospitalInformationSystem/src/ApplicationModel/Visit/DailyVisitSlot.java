/**
 * @(#) DailyVisitSlot.java
 */

package ApplicationModel.Visit;

import ApplicationModel.Medic.Doctor;

import java.util.Date;
import java.util.List;

public class DailyVisitSlot
{
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<VisitTimeSlot> getSlots() {
		return slots;
	}

	public void setSlots(List<VisitTimeSlot> slots) {
		this.slots = slots;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	private Date date;

	private List<VisitTimeSlot> slots;
	
	private Doctor doctor;
	
}
