/**
 * @(#) VisitCalendar.java
 */

package ApplicationModel.Visit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VisitCalendar
{
	private DailyVisitSlot slots;
	
	public static List<DailyVisitSlot> getDateSlots(String professionalID )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet result = null;
		ArrayList<DailyVisitSlot> output = new ArrayList<DailyVisitSlot>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection(
					"jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();
			String query =
					"SELECT * FROM VisitSlot  WHERE visitDate  > CURRENT_DATE " +
							"AND visitId IS NULL " +
							"AND doctorId = '" + professionalID + "' " +
							"ORDER BY visitDate, startTime";

			result = stmt.executeQuery(
					query);

			DailyVisitSlot currentDailySlot = null;
			List<VisitTimeSlot> currentTimeSlots = null;
			String currentDate = "";

			while(result.next()){
				String dateStr = result.getString("visitDate");
				if (!dateStr.equals(currentDate)) {
					currentDate = dateStr;
					if (currentDailySlot != null) {
						currentDailySlot.setSlots(currentTimeSlots);
						output.add(currentDailySlot);
					}
					currentDailySlot = new DailyVisitSlot();
					currentDailySlot.setDate(result.getDate("visitDate"));
					currentTimeSlots = new ArrayList<VisitTimeSlot>();
				}
				VisitTimeSlot ts = new VisitTimeSlot();
				ts.setTimeSlotId(result.getInt("slotId"));
				ts.setStartTime(result.getString("startTime"));
				ts.setEndTime(result.getString("endTime"));
				currentTimeSlots.add(ts);
			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return output;
	}
	
	public static boolean createNewVisit( int timeSlotId, String patientId, String followerProfessionalID )
	{
		Connection con = null;
		Statement stmt = null;
		ResultSet selectResult = null;
		int result = 0;
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
			stmt = con.createStatement();

			result = stmt.executeUpdate(String.format("INSERT INTO Visit (patientId, followerId, slotId) VALUES ('%s', '%s', %s)",
					patientId,
					followerProfessionalID,
					timeSlotId
			));
			con.commit();

			selectResult = stmt.executeQuery(String.format("SELECT * FROM Visit WHERE patientId = '%s' AND slotId = %s", patientId, timeSlotId));
			while(selectResult.next()){
				Integer visitId = selectResult.getInt("visitId");
				result = stmt.executeUpdate(String.format("UPDATE VisitSlot SET visitId = %s WHERE slotId = %s",
						visitId,
						timeSlotId
				));
				con.commit();
			}
		}
		catch (Exception e) {
			e.printStackTrace(System.out);
		}

		if(result == 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public void getPatientVisits( String idCode )
	{
		
	}
	
	public void updateVisitReport( String visitId, String report )
	{
		
	}
	
	
}
