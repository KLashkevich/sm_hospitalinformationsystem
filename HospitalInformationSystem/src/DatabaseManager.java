import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import Utilities.DateUtil;
import java.util.List;

public class DatabaseManager {
    public static void initializeDatabase(){
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();

            //-------------------------------reference table (for enumerations)-------------------------
            //Users an Roles
            stmt.executeUpdate("CREATE TABLE UserRole (roleID smallint NOT NULL, name varchar(50), PRIMARY KEY (roleID));");
            stmt.executeUpdate("INSERT INTO UserRole VALUES (0,'Administrative Officer')");
            stmt.executeUpdate("INSERT INTO UserRole VALUES (1,'Receptionist')");
            stmt.executeUpdate("INSERT INTO UserRole VALUES (2,'Doctor')");
            stmt.executeUpdate("INSERT INTO UserRole VALUES (3,'Surgeon')");
            //OncologistType
            stmt.executeUpdate("CREATE TABLE OncologistType (typeID smallint NOT NULL, name varchar(50), PRIMARY KEY (typeID));");
            stmt.executeUpdate("INSERT INTO OncologistType VALUES (0,'Medical')");
            stmt.executeUpdate("INSERT INTO OncologistType VALUES (1,'Radiation')");
            stmt.executeUpdate("INSERT INTO OncologistType VALUES (2,'Surgical')");
            stmt.executeUpdate("INSERT INTO OncologistType VALUES (3,'Gynecologic')");
            stmt.executeUpdate("INSERT INTO OncologistType VALUES (4,'Pediatric')");
            stmt.executeUpdate("INSERT INTO OncologistType VALUES (5,'Hematologist')");
            //surgeonType
            stmt.executeUpdate("CREATE TABLE SurgeonType (typeID smallint NOT NULL, name varchar(50), PRIMARY KEY (typeID));");
            stmt.executeUpdate("INSERT INTO SurgeonType VALUES (0,'General')");
            stmt.executeUpdate("INSERT INTO SurgeonType VALUES (1,'Pediatric')");
            stmt.executeUpdate("INSERT INTO SurgeonType VALUES (2,'Cardiothoracic')");
            stmt.executeUpdate("INSERT INTO SurgeonType VALUES (3,'Neurosurgery')");
            stmt.executeUpdate("INSERT INTO SurgeonType VALUES (4,'Oral and Maxillofacial')");
            stmt.executeUpdate("INSERT INTO SurgeonType VALUES (5,'Urology')");
            //testType
            stmt.executeUpdate("CREATE TABLE TestType (typeID smallint NOT NULL, name varchar(50), PRIMARY KEY (typeID));");
            stmt.executeUpdate("INSERT INTO TestType VALUES (0,'BloodTest')");
            stmt.executeUpdate("INSERT INTO TestType VALUES (1,'ImagingTest')");



            //User
            stmt.executeUpdate("CREATE TABLE User (" +
                    "userID int NOT NULL IDENTITY, " +
                    "username varchar(50) NOT NULL, " +
                    "password varchar(20) NOT NULL, " +
                    "roleID smallint NOT NULL, " +
                    "PRIMARY KEY (userID), " +
                    "FOREIGN KEY (roleID) REFERENCES UserRole(roleID))");
            //Oncologist
            stmt.executeUpdate("CREATE  TABLE Oncologist (" +
                    "professionalID varchar(20) NOT NULL, " +
                    "name varchar(50) NOT NULL, " +
                    "surname varchar(50) NOT NULL, " +
                    "careerLevel varchar(50) NOT NULL, " +
                    "oncologistTypeId smallint NOT NULL, " +
                    "PRIMARY KEY (professionalID), " +
                    "FOREIGN KEY (oncologistTypeId) REFERENCES OncologistType(typeID))");
            //Surgeon
            stmt.executeUpdate("CREATE  TABLE Surgeon (" +
                    "professionalID varchar(20) NOT NULL, " +
                    "name varchar(50) NOT NULL, " +
                    "surname varchar(50) NOT NULL, " +
                    "surgeonTypeId smallint NOT NULL, " +
                    "PRIMARY KEY (professionalID), " +
                    "FOREIGN KEY (surgeonTypeId) REFERENCES SurgeonType(typeID))");
            //Patient folder
            stmt.executeUpdate("CREATE TABLE PatientFolder (" +
                    "idCode varchar(20) NOT NULL, " +
                    "name varchar(50) NOT NULL, " +
                    "surname varchar(50) NOT NULL, " +
                    "birthDate DATE NOT NULL, " +
                    "insuranceType SMALLINT NOT NULL, " +
                    "insuranceCode varchar(50) NOT NULL, " +
                    "insuranceCompany varchar(50), " +
                    "firstVisitDate DATE, " +
                    "caseFollowerID varchar(20), " +
                    "catalogueCode varchar(50), " +
                    "PRIMARY KEY (idCode))");
            //TestSlot
            stmt.executeUpdate("CREATE TABLE TestSlot (" +
                    "slotId smallint NOT NULL IDENTITY, " +
                    "typeId smallint NOT NULL, " +
                    "testDate DATE NOT NULL, " +
                    "startTime varchar(5) NOT NULL, " +
                    "endTime varchar(5) NOT NULL, " +
                    "testId smallint, " +
                    "PRIMARY KEY (slotId), " +
                    "FOREIGN KEY (typeId) REFERENCES TestType(typeID))");
            //Test
            stmt.executeUpdate("CREATE TABLE Test (" +
                    "testId smallint NOT NULL IDENTITY, " +
                    "typeId smallint NOT NULL, " +
                    "patientId varchar(20) NOT NULL, " +
                    "slotId smallint, " +
                    "imagingTypeId smallint, " +
                    "PRIMARY KEY (testId), " +
                    "FOREIGN KEY (typeId) REFERENCES TestType(typeID), " +
                    "FOREIGN KEY (patientId) REFERENCES PatientFolder(idCode))");
            //Medicine
            stmt.executeUpdate("CREATE TABLE Medicine (" +
                    "medicineId varchar(20) NOT NULL, " +
                    "medicineName varchar(50) NOT NULL, " +
                    "pharmCompany varchar(20) NOT NULL, " +
                    "PRIMARY KEY (medicineId))");
            //SurgerySlot
            stmt.executeUpdate("CREATE TABLE SurgerySlot (" +
                    "slotId smallint NOT NULL IDENTITY, " +
                    "typeId smallint NOT NULL, " +
                    "surgeryDate DATE NOT NULL, " +
                    "surgeryId smallint, " +
                    "PRIMARY KEY (slotId), " +
                    "FOREIGN KEY (typeId) REFERENCES SurgeonType(typeID))");
            //Surgery
            stmt.executeUpdate("CREATE TABLE Surgery (" +
                    "surgeryId smallint NOT NULL IDENTITY, " +
                    "slotId smallint NOT NULL, " +
                    "surgeonId varchar(20) NOT NULL, " +
                    "patientId varchar(20) NOT NULL, " +
                    "PRIMARY KEY (surgeryId), " +
                    "FOREIGN KEY (slotId) REFERENCES SurgerySlot(slotId), " +
                    "FOREIGN KEY (patientId) REFERENCES PatientFolder(idCode), " +
                    "FOREIGN KEY (surgeonId) REFERENCES Surgeon(professionalID))");
            //SurgeryTeamParticipation
            stmt.executeUpdate("CREATE TABLE SurgeryTeamParticipation (" +
                    "participationID smallint NOT NULL IDENTITY, " +
                    "surgeryId smallint NOT NULL, " +
                    "professionalID varchar(20) NOT NULL )");
            //VisitSlot
            stmt.executeUpdate("CREATE TABLE VisitSlot (" +
                    "slotId smallint NOT NULL IDENTITY, " +
                    "doctorId varchar(20) NOT NULL, " +
                    "visitDate DATE NOT NULL, " +
                    "startTime varchar(5) NOT NULL, " +
                    "endTime varchar(5) NOT NULL, " +
                    "visitId smallint, " +
                    "PRIMARY KEY (slotId), " +
                    "FOREIGN KEY (doctorId) REFERENCES Oncologist(professionalID))");
            //Visit
            stmt.executeUpdate("CREATE TABLE Visit (" +
                    "visitId smallint NOT NULL IDENTITY, " +
                    "patientId varchar(20) NOT NULL, " +
                    "followerId varchar(20), " +
                    "slotId smallint, " +
                    "PRIMARY KEY (visitId), " +
                    "FOREIGN KEY (patientId) REFERENCES PatientFolder(idCode))");
            //TherapySlot
            stmt.executeUpdate("CREATE TABLE TherapySlot (" +
                    "slotId smallint NOT NULL IDENTITY, " +
                    "therapyDate DATE NOT NULL, " +
                    "availableTotalPlaces smallint NOT NULL, " +
                    "availableOverNightPlaces smallint NOT NULL, " +
                    "PRIMARY KEY (slotId))");
            //Therapy
            stmt.executeUpdate("CREATE TABLE Therapy (" +
                    "therapyId smallint NOT NULL IDENTITY, " +
                    "startDate DATE NOT NULL, " +
                    "endDate DATE NOT NULL, " +
                    "therapyTypeId smallint NOT NULL, " +
                    "patientId varchar(20) NOT NULL, " +
                    "PRIMARY KEY (therapyId), " +
                    "FOREIGN KEY (patientId) REFERENCES PatientFolder(idCode))");
            //Posology
            stmt.executeUpdate("CREATE TABLE Posology (" +
                    "posologyId smallint NOT NULL IDENTITY, " +
                    "startDate DATE NOT NULL, " +
                    "endDate DATE NOT NULL, " +
                    "unitsPerDay smallint NOT NULL, " +
                    "medicineId varchar(20) NOT NULL, " +
                    "therapyId smallint NOT NULL, " +
                    "PRIMARY KEY (posologyId), " +
                    "FOREIGN KEY (medicineId) REFERENCES Medicine(medicineId), " +
                    "FOREIGN KEY (therapyId) REFERENCES Therapy(therapyId))");

            //-----------------------------slots generation-------------------------
            String[] startTimes = {"09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00",
                    "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30"};
            String[] endTimes = {"09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30",
                    "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00"};
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String stringDate;

            //Slots for Tests
            Date currentDate = Calendar.getInstance().getTime();
            for (int i = 1; i <= 31; i++) {
                currentDate = DateUtil.addDays(currentDate, 1);
                stringDate = df.format(currentDate);
                for (int j = 0; j < 20; j++) {
                    stmt.executeUpdate(String.format("INSERT INTO TestSlot (typeId, testDate, startTime, endTime)\n" +
                            "VALUES (0, '%s', '%s', '%s')", stringDate, startTimes[j], endTimes[j]));
                    stmt.executeUpdate(String.format("INSERT INTO TestSlot (typeId, testDate, startTime, endTime)\n" +
                            "VALUES (1, '%s', '%s', '%s')", stringDate, startTimes[j], endTimes[j]));
                }
            }
            //Slots for surgeries
            currentDate = Calendar.getInstance().getTime();
            for (int i = 1; i <= 31; i++) {
                currentDate = DateUtil.addDays(currentDate, 1);
                stringDate = df.format(currentDate);
                for (int j = 0; j < 6; j++) { //for all surgery types we add one slot per day
                    stmt.executeUpdate(String.format("INSERT INTO SurgerySlot (typeId, surgeryDate)\n" +
                            "VALUES (%s, '%s')", j, stringDate));
                    stmt.executeUpdate(String.format("INSERT INTO SurgerySlot (typeId, surgeryDate)\n" +
                            "VALUES (%s, '%s')", j, stringDate));
                }
            }
            //slots for therapy
            currentDate = Calendar.getInstance().getTime();
            for (int i = 1; i <= 101; i++) {
                currentDate = DateUtil.addDays(currentDate, 1);
                stringDate = df.format(currentDate);
                stmt.executeUpdate(String.format("INSERT INTO TherapySlot (therapyDate, availableTotalPlaces, availableOverNightPlaces)\n" +
                        "VALUES ('%s', 20, 10)", stringDate));
            }

            System.out.println("Database initialized successfully");
        }catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public static void resetDatabase(){
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            stmt.executeUpdate("DROP SCHEMA PUBLIC CASCADE;");
            System.out.println("Database reset successfully");
        }  catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}