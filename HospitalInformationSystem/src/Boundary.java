/**
 * @(#) Boundary.java
 */
import ApplicationModel.Medic.*;
import ApplicationModel.Medicine.Medicine;
import ApplicationModel.Patient.PatientFolder;
import ApplicationModel.Surgery.SurgeryDailySlot;
import ApplicationModel.Tests.ImagingTestType;
import ApplicationModel.Tests.TestDailySlot;
import ApplicationModel.Tests.TestTimeSlot;
import ApplicationModel.Therapy.Posology;
import ApplicationModel.Therapy.Therapy;
import ApplicationModel.User;
import ApplicationModel.UserRole;
import ApplicationModel.Visit.DailyVisitSlot;
import ApplicationModel.Visit.VisitTimeSlot;
import InteractionModel.Controller;
import InteractionModel.TherapyFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Boundary
{
    public static Map returnCareerMap()
    {
        Map<String, String> careerMap = new HashMap<String, String>();
        careerMap.put("ApplicationModel.Medic.MedicalStudent", "Medical Student");
        careerMap.put("ApplicationModel.Medic.PhysicianAssistant", "Physician Assistant");
        careerMap.put("ApplicationModel.Medic.Fellow", "Fellow");
        careerMap.put("ApplicationModel.Medic.Specialist", "Specialist");
        return careerMap;
    }

    public static void createNewOncologist()
    {
        String professionalID, firstName, surname;
        OncologistType oncologistType;
        Scanner scan= new Scanner(System.in);
        String[] careerLevels = {"Medical Student", "Physician Assistant", "Fellow", "Specialist"};

        System.out.println("CREATE NEW ONCOLOGIST\nEnter professionalID:");
        professionalID = scan.nextLine();
        System.out.println("Enter first name:");
        firstName = scan.nextLine();
        System.out.println("Enter surname:");
        surname = scan.nextLine();
        System.out.println("Select oncologist type:");
        System.out.println("0. Medical\n1. Radiation\n2. Surgical\n3. Gynecologic\n4. Pediatric\n5. Hematologist");
        oncologistType = OncologistType.values()[new Integer(scan.nextLine())];
        System.out.println("Select the career level:");
        System.out.println("0. Medical Student\n1. Physician Assistant\n2. Fellow\n3. Specialist");
        String carLevel = careerLevels[new Integer(scan.nextLine())];
        boolean isOncologistAdded = false;
        try {
            isOncologistAdded = Controller.addOncologist(firstName,surname,oncologistType,carLevel,professionalID);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        if (isOncologistAdded) {
            System.out.println(String.format("The new %s, %s oncologist %s %s is added with id %s",
                    oncologistType, carLevel, firstName, surname, professionalID));
        }
        else {
            System.out.println("DB issue occured. Please try again");
        }
    }

    public static void viewAllOncologists()
    {
        String className;
        Map<String, String> careerMap = returnCareerMap();

        List<Oncologist> oncologists = Controller.getOncologists(null);
        System.out.println("ONCOLOGISTS:");
        for (Oncologist oncologist: oncologists) {
            className = oncologist.getClass().getName();
            System.out.println(String.format("[%s]: %s %s, type: %s, career level: %s", oncologist.getProfessionalID(),
                    oncologist.getName(), oncologist.getSurname(), oncologist.getType(), careerMap.get(className)));
    }

}

    public static void createNewSurgeon()
    {
        String professionalID, firstName, surname;
        SurgeonType surgeonType;
        Scanner scan= new Scanner(System.in);

        System.out.println("CREATE NEW Surgeon\nEnter professionalID:");
        professionalID = scan.nextLine();
        System.out.println("Enter first name:");
        firstName = scan.nextLine();
        System.out.println("Enter surname:");
        surname = scan.nextLine();
        System.out.println("Select surgeon type:");

        System.out.println("0. General\n1. Pediatric\n2. Cardiothoracic\n3. Neurosurgery\n4. Oral and Maxillofacial\n5. Urology");
        surgeonType = SurgeonType.values()[new Integer(scan.nextLine())];

        boolean isSurgeonAdded = false;
        try {
            isSurgeonAdded = Controller.addSurgeon(firstName, surname, surgeonType, professionalID);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        if (isSurgeonAdded) {
            System.out.println(String.format("The new %s surgeon %s %s is added with id %s",
                    surgeonType, firstName, surname, professionalID));
        }
        else {
            System.out.println("DB issue occured. Please try again");
        }
    }

    public static void viewAllSurgeons()
    {
        List<Surgeon> surgeons = Controller.getSurgeons();
        System.out.println("SURGEONS:");
        for (Surgeon surgeon: surgeons) {
            System.out.println(String.format("[%s]: %s %s, type: %s", surgeon.getProfessionalID(),
                    surgeon.getName(), surgeon.getSurname(), surgeon.getType()));
        }
    }

    public static void createNewPatientFolder()
    {
        String professionalID, className, idCode, patientName, patientSurname, insCode, insCompany, dateStr;
        int insuranceType = 0;
        Date birthDate, firstVisitDate;
        Map<String, String> careerMap = returnCareerMap();
        Scanner scan= new Scanner(System.in);


        System.out.println("CREATE NEW PATIENT FOLDER\nEnter ID Code:");
        idCode = scan.nextLine();
        System.out.println("Enter first name:");
        patientName = scan.nextLine();
        System.out.println("Enter surname:");
        patientSurname = scan.nextLine();
        System.out.println("Enter birth date (dd-MM-yyyy):");
        dateStr = scan.nextLine();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        try {
            birthDate = format.parse(dateStr);
        } catch (ParseException e) {
            birthDate = new GregorianCalendar(1990, 1, 1).getTime();
        }
        System.out.println("Select insurance type:");
        System.out.println("0. National\n1. Private");
        insCode = null;
        insCompany = null;
        insuranceType = new Integer(scan.nextLine());
        if (insuranceType == 1) {
            System.out.println("Enter insurance code:");
            insCode = scan.nextLine();
            System.out.println("Enter insurance company");
            insCompany = scan.nextLine();
        }
        boolean isPatientFolderAdded = false;
        try {
            isPatientFolderAdded = Controller.createPatientFolder(patientName, patientSurname, idCode, birthDate, insuranceType, insCode, insCompany);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        if (isPatientFolderAdded) {
            System.out.println(String.format("The new patient folder for the patient [%s] %s %s is created",
                    idCode, patientName, patientSurname));
            System.out.println("Do you want to assign a case follower specialist? (Y/N)");
            if(scan.nextLine().equals("Y")){
                List<Oncologist> oncologists = Controller.getOncologists("Specialist");
                if (oncologists.size() == 0) {
                    System.out.println("There are no specialists in the database");
                }
                else {
                    System.out.println("Select the professional ID from the following: ");

                    List<String> ids = new ArrayList<String>();

                    System.out.println("SPECIALISTS:");
                    for (Oncologist oncologist: oncologists) {
                        ids.add(oncologist.getProfessionalID());
                        className = oncologist.getClass().getName();
                        System.out.println(String.format("[%s]: %s %s, type: %s, career level: %s", oncologist.getProfessionalID(),
                                oncologist.getName(), oncologist.getSurname(), oncologist.getType(), careerMap.get(className)));
                    }
                    while(true) {
                        professionalID = scan.nextLine();
                        if (!ids.contains(professionalID)) {
                            System.out.println("There is no such specialist code!");
                        }
                        else {
                            break;
                        }
                    }
                    if (Controller.assignOncologist(idCode, professionalID)) {
                        System.out.println(String.format("Specialist %s is assigned as a case follower for the patient %s", professionalID, idCode));
                    }
                    else {
                        System.out.println("DB issue occured. Please try again");
                    }
                }
            }
            System.out.println("Do you want to assign the first visit date? (Y/N)");
            if(scan.nextLine().equals("Y")){
                System.out.println("Enter the first visit date (dd-MM-yyyy):");

                dateStr = scan.nextLine();
                try {
                    firstVisitDate = format.parse(dateStr);
                } catch (ParseException e) {
                    firstVisitDate = new GregorianCalendar(1990, 1, 1).getTime();
                }

                if (Controller.setFirstVisitDate(idCode, firstVisitDate)) {
                    System.out.println(String.format("First visit date for the patient %s is set as %s", idCode, dateStr));
                }
                else {
                    System.out.println("DB issue occured. Please try again");
                }
            }
        }
        else {
            System.out.println("DB issue occured. Please try again");
        }
    }

    public static void viewAllPatients()
    {
        List<PatientFolder> folders;

        folders = Controller.getPatients();
        System.out.println("PATIENTS:");
        for (PatientFolder folder : folders){
            System.out.println(String.format("[%s]: %s %s, insurance type: %s, insurance code %s, birth date %s",
                    folder.getPatient().getIdCode(),
                    folder.getPatient().getName(),
                    folder.getPatient().getSurname(),
                    folder.getPatient().getClass().getSimpleName(),
                    folder.getPatient().getInsuranceCode(),
                    folder.getPatient().getBirthDate()
            ));
        }
    }

    public static void bookImagingTest()
    {
        ImagingTestType imgTestType;
        List<PatientFolder> folders;
        String idCode;

        System.out.println("Do you want to initialize the database? (Y/N)");
        Scanner scan= new Scanner(System.in);


        System.out.println("IMAGING TEST BOOKING");
        System.out.println("Select the patient ID code from the following: ");

        List<String> ids = new ArrayList<String>();
        folders = Controller.getPatients();
        System.out.println("PATIENTS:");
        for (PatientFolder folder : folders){
            System.out.println(String.format("[%s]: %s %s, insurance type: %s, insurance code %s, birth date %s",
                    folder.getPatient().getIdCode(),
                    folder.getPatient().getName(),
                    folder.getPatient().getSurname(),
                    folder.getPatient().getClass().getSimpleName(),
                    folder.getPatient().getInsuranceCode(),
                    folder.getPatient().getBirthDate()
            ));
            ids.add(folder.getPatient().getIdCode());
        }
        while(true) {
            idCode = scan.nextLine();
            if (!ids.contains(idCode)) {
                System.out.println("There is no such patient ID code!");
            }
            else {
                break;
            }
        }
        System.out.println("Select imaging test type:");

        System.out.println("0. XRay\n1. PET\n2. TC\n3. MRI");
        imgTestType = ImagingTestType.values()[new Integer(scan.nextLine())];

        List<TestDailySlot> slots = Controller.getImgTestsDaySlots(null);
        List<Integer> slotsNumbers = new ArrayList<Integer>();
        if (slots != null && slots.size() > 0){
            System.out.println("AVAILABLE DATES:");
            int i = 0;
            for (TestDailySlot ds : slots) {
                System.out.println(String.format("[%s]: %tF", i, ds.getDate()));
                slotsNumbers.add(i);
                i++;
            }
            System.out.println("Select the date slot number");
            int slotNumber;
            while (true) {
                slotNumber = new Integer(scan.nextLine());
                if (!slotsNumbers.contains(slotNumber)) {
                    System.out.println("There is no such slot number!");
                }
                else {
                    break;
                }
            }
            //show time slots for
            TestDailySlot selectedSlot = slots.get(slotNumber);
            //int j = 0;
            List<Short> timeSlotsIds = new ArrayList<Short>();
            System.out.println("AVAILABLE TIME SLOTS:");
            for (TestTimeSlot ts : selectedSlot.getSlots()) {
                System.out.println(String.format("[%s]: %s - %s", ts.getTimeSlotId(), ts.getStartTime(), ts.getEndTime()));
                timeSlotsIds.add(ts.getTimeSlotId());
            }

            short timeSlotNumber;
            System.out.println("Select the time slot number");
            while (true) {
                timeSlotNumber = new Short(scan.nextLine());
                if (!timeSlotsIds.contains(timeSlotNumber)) {
                    System.out.println("There is no such slot number!");
                }
                else {
                    break;
                }
            }
            if (Controller.bookImgTestDateTime(idCode, timeSlotNumber, imgTestType)) {
                System.out.println(String.format("The new %s imaging test for patient [%s] is booked",
                        imgTestType, idCode));
            }
            else {
                System.out.println("DB issue occured. Please try again");
            }
        }
        else {
            System.out.println("There are no available time slots now!");
        }
    }

    public static void bookBloodTest()
    {
        List<String> patientIds;
        List<PatientFolder> folders;
        List<TestDailySlot> testDailySlots;
        List<Integer> tDailySlotsNumbers;
        String idCode;
        Scanner scan= new Scanner(System.in);

        System.out.println("BLOOD TEST BOOKING");
        System.out.println("Select the patient ID code from the following: ");

        patientIds = new ArrayList<String>();
        folders = Controller.getPatients();
        System.out.println("PATIENTS:");
        for (PatientFolder folder : folders){
            System.out.println(String.format("[%s]: %s %s, insurance type: %s, insurance code %s, birth date %s",
                    folder.getPatient().getIdCode(),
                    folder.getPatient().getName(),
                    folder.getPatient().getSurname(),
                    folder.getPatient().getClass().getSimpleName(),
                    folder.getPatient().getInsuranceCode(),
                    folder.getPatient().getBirthDate()
            ));
            patientIds.add(folder.getPatient().getIdCode());
        }
        while(true) {
            idCode = scan.nextLine();
            if (!patientIds.contains(idCode)) {
                System.out.println("There is no such patient ID code!");
            }
            else {
                break;
            }
        }

        testDailySlots = Controller.getBlTestsDaySlots(null);
        tDailySlotsNumbers = new ArrayList<Integer>();
        if (testDailySlots != null && testDailySlots.size() > 0){
            System.out.println("AVAILABLE DATES:");
            int i = 0;
            for (TestDailySlot ds : testDailySlots) {
                System.out.println(String.format("[%s]: %tF", i, ds.getDate()));
                tDailySlotsNumbers.add(i);
                i++;
            }
            System.out.println("Select the date slot number");
            int slotNumber;
            while (true) {
                slotNumber = new Integer(scan.nextLine());
                if (!tDailySlotsNumbers.contains(slotNumber)) {
                    System.out.println("There is no such slot number!");
                }
                else {
                    break;
                }
            }
            TestDailySlot selectedSlot = testDailySlots.get(slotNumber);
            //int j = 0;
            List<Short> timeSlotsIds = new ArrayList<Short>();
            System.out.println("AVAILABLE TIME SLOTS:");
            for (TestTimeSlot ts : selectedSlot.getSlots()) {
                System.out.println(String.format("[%s]: %s - %s", ts.getTimeSlotId(), ts.getStartTime(), ts.getEndTime()));
                timeSlotsIds.add(ts.getTimeSlotId());
            }

            short timeSlotNumber;
            System.out.println("Select the time slot number");
            while (true) {
                timeSlotNumber = new Short(scan.nextLine());
                if (!timeSlotsIds.contains(timeSlotNumber)) {
                    System.out.println("There is no such slot number!");
                }
                else {
                    break;
                }
            }
            if (Controller.bookBlTestDateTime(idCode, timeSlotNumber)) {
                System.out.println(String.format("The new blood test for patient [%s] is booked",
                        idCode));
            }
            else {
                System.out.println("DB issue occured. Please try again");
            }
        }
        else {
            System.out.println("There are no available time slots now!");
        }
    }

    public static void surgeryBooking()
    {
        SurgeonType surgeonType;
        List<String> patientIds;
        List<PatientFolder> folders;
        List<SurgeryDailySlot> surgeryDailySlots;
        List<Integer> sDailySlotsNumbers;
        String idCode;
        Scanner scan= new Scanner(System.in);

        System.out.println("SURGERY BOOKING");
        System.out.println("Select the patient ID code from the following: ");

        patientIds = new ArrayList<String>();
        folders = Controller.getPatients();
        System.out.println("PATIENTS:");
        for (PatientFolder folder : folders){
            System.out.println(String.format("[%s]: %s %s, insurance type: %s, insurance code %s, birth date %s",
                    folder.getPatient().getIdCode(),
                    folder.getPatient().getName(),
                    folder.getPatient().getSurname(),
                    folder.getPatient().getClass().getSimpleName(),
                    folder.getPatient().getInsuranceCode(),
                    folder.getPatient().getBirthDate()
            ));
            patientIds.add(folder.getPatient().getIdCode());
        }
        while(true) {
            idCode = scan.nextLine();
            if (!patientIds.contains(idCode)) {
                System.out.println("There is no such patient ID code!");
            }
            else {
                break;
            }
        }

        System.out.println("Select surgeon type:");

        System.out.println("0. General\n1. Pediatric\n2. Cardiothoracic\n3. Neurosurgery\n4. Oral and Maxillofacial\n5. Urology");
        surgeonType = SurgeonType.values()[new Integer(scan.nextLine())];

        surgeryDailySlots = Controller.getSurgeryDaySlots(surgeonType, null);

        System.out.println("AVAILABLE DATE SLOTS:");
        sDailySlotsNumbers = new ArrayList<Integer>();
        for (SurgeryDailySlot ss : surgeryDailySlots) {
            System.out.println(String.format("[%s]: %tF %s", ss.getSlotId(), ss.getDate(), ss.getSlotType()));
            sDailySlotsNumbers.add(ss.getSlotId());
        }
        System.out.println("Select the date slot number");
        int slotNumber;
        while (true) {
            slotNumber = new Integer(scan.nextLine());
            if (!sDailySlotsNumbers.contains(slotNumber)) {
                System.out.println("There is no such slot number!");
            }
            else {
                break;
            }
        }
        SurgeryDailySlot selectedSlot = surgeryDailySlots.get(slotNumber);
        List<Surgeon> availableSurgeons = Controller.getAvailableSurgeons(surgeonType, selectedSlot.getDate());
        List<String> surgeonIds = new ArrayList<String>();

        if (availableSurgeons.size() == 0) {
            System.out.println(String.format("There are no available %s surgeons for the selected date!", surgeonType));
        }
        else {
            String selectedSurgeonStr;
            System.out.println("Select the surgeon ID:");
            for (Surgeon surgeon: availableSurgeons) {
                System.out.println(String.format("[%s]: %s %s, type: %s", surgeon.getProfessionalID(),
                        surgeon.getName(), surgeon.getSurname(), surgeon.getType()));
                surgeonIds.add(surgeon.getProfessionalID());
            }
            while (true) {
                selectedSurgeonStr = scan.nextLine();
                if (!surgeonIds.contains(selectedSurgeonStr)) {
                    System.out.println("There is no such surgeon ID!");
                }
                else {
                    break;
                }
            }
            if (Controller.bookSurgeryDate(slotNumber, selectedSurgeonStr, idCode)) {
                System.out.println(String.format("The new surgery for patient [%s] is booked",
                        idCode));
            }
            else {
                System.out.println("DB issue occured. Please try again");
            }
        }
    }

    public static void visitBooking()
    {
        String className, idCode;
        List<String> patientIds;
        List<PatientFolder> folders;
        List<DailyVisitSlot> visitDailySlots;
        List<Integer> tDailySlotsNumbers;
        Map<String, String> careerMap = returnCareerMap();
        Scanner scan= new Scanner(System.in);


        System.out.println("VISIT BOOKING");
        System.out.println("Select the patient ID code from the following: ");
        patientIds = new ArrayList<String>();
        folders = Controller.getPatients();
        System.out.println("PATIENTS:");
        for (PatientFolder folder : folders){
            System.out.println(String.format("[%s]: %s %s, insurance type: %s, insurance code %s, birth date %s",
                    folder.getPatient().getIdCode(),
                    folder.getPatient().getName(),
                    folder.getPatient().getSurname(),
                    folder.getPatient().getClass().getSimpleName(),
                    folder.getPatient().getInsuranceCode(),
                    folder.getPatient().getBirthDate()
            ));
            patientIds.add(folder.getPatient().getIdCode());
        }
        while(true) {
            idCode = scan.nextLine();
            if (!patientIds.contains(idCode)) {
                System.out.println("There is no such patient ID code!");
            }
            else {
                break;
            }
        }
        List<Oncologist> doctors = Controller.getAvailableDoctors();
        if (doctors.size() == 0 ){
            System.out.println("There are no available doctors, try later");
            return;
        }
        List<String> doctorIds = new ArrayList<String>();
        System.out.println("Select Host Doctor:");
        for (Oncologist oncologist: doctors) {
            className = oncologist.getClass().getName();
            System.out.println(String.format("[%s]: %s %s, type: %s, career level: %s", oncologist.getProfessionalID(),
                    oncologist.getName(), oncologist.getSurname(), oncologist.getType(), careerMap.get(className)));
            doctorIds.add(oncologist.getProfessionalID());
        }
        String hostId, followerId;
        while(true) {
            hostId = scan.nextLine();
            if (!doctorIds.contains(hostId)) {
                System.out.println("There is no such professional ID code!");
            }
            else {
                break;
            }
        }
        System.out.println("Select Follower Doctor:");
        while(true) {
            followerId = scan.nextLine();
            if (!doctorIds.contains(followerId)) {
                System.out.println("There is no such professional ID code!");
            }
            else {
                break;
            }
        }
        visitDailySlots = Controller.getDateSlots(hostId);
        tDailySlotsNumbers = new ArrayList<Integer>();
        if (visitDailySlots != null && visitDailySlots.size() > 0){
            System.out.println("AVAILABLE VISIT DATES:");
            int i = 0;
            for (DailyVisitSlot vs : visitDailySlots) {
                System.out.println(String.format("[%s]: %tF", i, vs.getDate()));
                tDailySlotsNumbers.add(i);
                i++;
            }
            System.out.println("Select the date slot number");
            int vslotNumber;
            while (true) {
                vslotNumber = new Integer(scan.nextLine());
                if (!tDailySlotsNumbers.contains(vslotNumber)) {
                    System.out.println("There is no such slot number!");
                }
                else {
                    break;
                }
            }
            DailyVisitSlot selectedVisitSlot = visitDailySlots.get(vslotNumber);
            List<Integer> timeSlotsIds = new ArrayList<Integer>();
            System.out.println("AVAILABLE TIME SLOTS:");
            for (VisitTimeSlot ts : selectedVisitSlot.getSlots()) {
                System.out.println(String.format("[%s]: %s - %s", ts.getTimeSlotId(), ts.getStartTime(), ts.getEndTime()));
                timeSlotsIds.add(ts.getTimeSlotId());
            }
            Integer timeSlotNumber;
            System.out.println("Select the time slot number");
            while (true) {
                timeSlotNumber = new Integer(scan.nextLine());
                if (!timeSlotsIds.contains(timeSlotNumber)) {
                    System.out.println("There is no such slot number!");
                }
                else {
                    break;
                }
            }
            if (Controller.bookVisitDateAndTime(timeSlotNumber, idCode, followerId)) {
                System.out.println(String.format("The new visit for patient [%s] is booked",
                        idCode));
            }
            else {
                System.out.println("DB issue occured. Please try again");
            }
        }
        else {
            System.out.println("There are no available time slots now!");
        }
    }

    public static void setCatalogueCode()
    {
        Scanner scan= new Scanner(System.in);
        String patientId, code;

        System.out.println("SET CATALOGUE CODE\nEnter patientId:");
        patientId = scan.nextLine();
        System.out.println("Enter catalogue code:");
        code = scan.nextLine();
        boolean isCatalogueSet = false;
        try {
            isCatalogueSet = Controller.setCatalogueCode(patientId, code);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        if (isCatalogueSet) {
            System.out.println(String.format("The new %s catalogue code is set to %s patient",
                    code, patientId));
        }
        else {
            System.out.println("DB issue occured. Please try again");
        }
    }

    public static void addMedicine()
    {
        Scanner scan= new Scanner(System.in);
        String  medcode, medname, company;

        System.out.println("ADD MEDICINE\nEnter medicine code:");
        medcode = scan.nextLine();
        System.out.println("Enter medicine name:");
        medname = scan.nextLine();
        System.out.println("Enter pharmaceutical company:");
        company = scan.nextLine();

        boolean isMedicineAdded = false;
        try {
            isMedicineAdded = Controller.addMedicine(medcode, medname, company);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        if(isMedicineAdded){
            System.out.println(String.format("The new medicine %s with code %s from %s was added",
                    medname, medcode, company));
        }
        else {
            System.out.println("DB issue occured. Please try again");
        }
    }

    public static void viewAllMedicines()
    {
        List<Medicine> medicines = Controller.getMedicines();
        System.out.println("MEDICINES:");
        for (Medicine medicine : medicines){
            System.out.println(String.format("[%s]: %s, company: %s",
                    medicine.getCode(),
                    medicine.getName(),
                    medicine.getPharmaceuticalCompany()));
        }
    }

    public static void createNewTherapy()
    {
        List<String> patientIds;
        List<PatientFolder> folders;
        String idCode;
        Scanner scan= new Scanner(System.in);



        System.out.println("CREATE NEW THERAPY");
        System.out.println("Select the patient ID code from the following: ");
        patientIds = new ArrayList<String>();


        folders = Controller.getPatients();
        System.out.println("PATIENTS:");
        for (PatientFolder folder : folders){
            System.out.println(String.format("[%s]: %s %s, insurance type: %s, insurance code %s, birth date %s",
                    folder.getPatient().getIdCode(),
                    folder.getPatient().getName(),
                    folder.getPatient().getSurname(),
                    folder.getPatient().getClass().getSimpleName(),
                    folder.getPatient().getInsuranceCode(),
                    folder.getPatient().getBirthDate()
            ));
            patientIds.add(folder.getPatient().getIdCode());
        }

        while(true) {
            idCode = scan.nextLine();
            if (!patientIds.contains(idCode)) {
                System.out.println("There is no such patient ID code!");
            }
            else {
                break;
            }
        }
        String therapyStartStr, therapyEndStr;
        Date therapyStartDate, therapyEndDate;
        short therapyType;

        while (true) {
            System.out.println("Enter therapy start date (dd-MM-yyyy):");
            therapyStartStr = scan.nextLine();
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            try {
                therapyStartDate = format.parse(therapyStartStr);
            } catch (ParseException e) {
                therapyStartDate = new GregorianCalendar(2019, 1, 1).getTime();
            }
            System.out.println("Enter therapy end date (dd-MM-yyyy):");
            therapyEndStr = scan.nextLine();
            try {
                therapyEndDate = format.parse(therapyEndStr);
            } catch (ParseException e) {
                therapyEndDate = new GregorianCalendar(2019, 1, 5).getTime();
            }

            if (therapyEndDate.before(therapyStartDate)) {
                System.out.println("ERROR: End date is earlier than start date");
                continue;
            }

            if (Controller.checkTherapyOverlapping(idCode, therapyStartDate, therapyEndDate)){
                System.out.println("ERROR: This therapy is overlapping with another therapy dates");
                continue;
            }

            System.out.println("Select therapy type:");

            System.out.println("1. Home\n2. Day Hospital Therapy\n3. Hospital overnight therapy");

            try {
                therapyType = new Short(scan.nextLine());
            }
            catch (Exception e) {
                System.out.println("Incorrect input");
                continue;
            }
            if (therapyType < 1 ||therapyType > 3) {
                System.out.println("Incorrect input. Selecting home therapy by default");
                therapyType = 1;
            }

            if (therapyType == 2 && !therapyEndDate.equals(therapyStartDate)){
                System.out.println("Day hospital therapy can last only one day.");
                therapyEndDate = therapyStartDate;
            }

            if (!Controller.isAvailableTherapyPeriod(therapyType, therapyStartDate, therapyEndDate)) {
                System.out.println("Such date period is occupied or does not exist.");
                continue;
            }
            break;
        }


        List<Medicine> therapyMedicines = Controller.getMedicines();

        List<Integer> medicineNumbers = new ArrayList<Integer>();
        System.out.println("MEDICINES:");
        int medIndex = 0;
        for (Medicine medicine : therapyMedicines){
            System.out.println(String.format("[%s] [%s]: %s, company: %s",
                    medIndex,
                    medicine.getCode(),
                    medicine.getName(),
                    medicine.getPharmaceuticalCompany()));
            medicineNumbers.add(medIndex);
            medIndex++;
        }

        if (therapyMedicines.size() == 0){
            System.out.println("There are no medicines added to the database!");
            return;
        }

        Therapy therapy = TherapyFactory.getTherapy(therapyType);
        therapy.setPatientId(idCode);
        therapy.setPosologies(new HashMap<Medicine, Posology>());
        therapy.setStartDate(therapyStartDate);
        therapy.setEndDate(therapyEndDate);


        Date medicineStartDate, medicineEndDate;
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        while (true) {
            System.out.println("Select an option:\n1. Add medicine to the therapy\n2. END");
            int medChoice = new Integer(scan.nextLine());
            if (medChoice == 2) {
                if (therapy.getPosologies().size() == 0) {
                    System.out.println("You should add at least one medicine!");
                    continue;
                }
                else {
                    break;
                }
            }
            else { //Adding a new medicine to the list
                System.out.println("Enter the medicine number");
                int medNum = new Integer(scan.nextLine());
                if (!medicineNumbers.contains(medNum)) {
                    System.out.println("There is no such medicine number!");
                    continue;
                }
                Medicine currentMedicine = therapyMedicines.get(medNum);

                if (therapy.containsMedicine(currentMedicine.getCode())) {
                    System.out.println("Such medicine is already added!");
                    continue;
                }

                System.out.println("Enter the medicine start date (dd-MM-yyyy):");
                String medinineStartStr, medinineEndStr;
                short unitsPerDay;
                medinineStartStr = scan.nextLine();
                try {
                    medicineStartDate = format.parse(medinineStartStr);
                }
                catch (Exception e) {
                    System.out.println("Incorrect date format!");
                    continue;
                }

                System.out.println("Enter the medicine end date (dd-MM-yyyy):");
                medinineEndStr = scan.nextLine();
                try {
                    medicineEndDate = format.parse(medinineEndStr);
                }
                catch (Exception e) {
                    System.out.println("Incorrect date format!");
                    continue;
                }

                System.out.println("Enter the number of units per day:");
                unitsPerDay = new Short(scan.nextLine());

                Posology posology = new Posology();
                posology.setUnitsPerDay(unitsPerDay);
                posology.setStartDate(medicineStartDate);
                posology.setEndDate(medicineEndDate);

                therapy.addMedicine(currentMedicine, posology);
            }
        }

        if (Controller.createTherapy(therapyType, therapy)) {
            System.out.println("The new therapy has been saved");
        }
        else {
            System.out.println("DB issue occured. Please try again");
        }
    }

    public static void deletePatientFolder()
    {
        String idCode;
        List<String> patientIds;
        List<PatientFolder> folders;
        Scanner scan= new Scanner(System.in);


        System.out.println("DELETE PATIENT'S FOLDER");
        System.out.println("Select the patient ID code from the following: ");

        patientIds = new ArrayList<String>();
        folders = Controller.getPatients();
        System.out.println("PATIENTS:");
        for (PatientFolder folder : folders){
            System.out.println(String.format("[%s]: %s %s, insurance type: %s, insurance code %s, birth date %s",
                    folder.getPatient().getIdCode(),
                    folder.getPatient().getName(),
                    folder.getPatient().getSurname(),
                    folder.getPatient().getClass().getSimpleName(),
                    folder.getPatient().getInsuranceCode(),
                    folder.getPatient().getBirthDate()
            ));
            patientIds.add(folder.getPatient().getIdCode());
        }
        while(true) {
            idCode = scan.nextLine();
            if (!patientIds.contains(idCode)) {
                System.out.println("There is no such patient ID code!");
            }
            else {
                break;
            }
        }

        boolean deleted = false;
        try {
            deleted = Controller.deletePatientFolder(idCode);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        if(deleted){
            System.out.println(String.format("The patients folder is deleted. Id %s",
                    idCode));
        }
        else {
            System.out.println("DB issue occured. Please try again");
        }
    }


    public static void main(String[] args) {
        UserRole role;
        String username, password;
        Scanner scan= new Scanner(System.in);

        while(true){
            System.out.println("Select an option:");
            System.out.println("1. Login");
            System.out.println("2. Register");
            System.out.println("3. Reset database");
            int choice = new Integer(scan.nextLine());

            menuloop:
            switch (choice){
                case 1:
//                    ---------------------------login------------------------
                    System.out.println("Insert username:");
                    username = scan.nextLine();
                    System.out.println("Insert password:");
                    password = scan.nextLine();
                    User user = null;
                    try {
                        user = Controller.loginUser(username, password);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        continue;
                    }

                    if (user == null) {
                        System.out.println("The password is incorrect!");
                    }
                    else {
                        System.out.println(String.format("Signed in as %s in role %s", user.getName(), user.getRole()));
                        while (true) {
                            switch (user.getRole()){
                                case AdminOfficer:
                                    System.out.println("Select an option:");
                                    System.out.println("1. Add new oncologist\n2. View all oncologists\n3. Add new surgeon" +
                                            "\n4. View all surgeons\n5. Delete patient folder\n6. Log out");
                                    choice = new Integer(scan.nextLine());
                                    switch (choice){
                                        case 1:
                                            createNewOncologist();
                                            break;
                                        case 2:
                                            viewAllOncologists();
                                            break;
                                        case 3:
                                            createNewSurgeon();
                                            break;
                                        case 4:
                                            viewAllSurgeons();
                                            break;
                                        case 5:
                                            deletePatientFolder();
                                            break;
                                        case 6:
                                            break menuloop;
                                        default:
                                            break;
                                    }
                                    break;
                                case Receptionist:
                                    System.out.println("Select an option:");
                                    System.out.println("1. Create New Patient Folder\n2. View All Patients\n3. Log out");
                                    choice = new Integer(scan.nextLine());
                                    switch (choice){
                                        case 1:
                                            createNewPatientFolder();
                                            break;
                                        case 2:
                                            viewAllPatients();
                                            break;
                                        case 3:
                                            break menuloop;
                                        default:
                                            break;
                                    }
                                    break;
                                case Doctor:
                                    System.out.println("Select an option:");
                                    System.out.println("1. Book Imaging Test\n2. Book Blood Test\n3. Book Surgery" +
                                            "\n4. Book Visit\n5. View Patient Folder Info\n6. Set Catalogue Code" +
                                            "\n7. Add Medicine\n8. View All Medicines\n9. Create Therapy\n10. Log out");
                                    choice = new Integer(scan.nextLine());
                                    switch (choice){
                                        case 1:
                                            bookImagingTest();
                                            break;
                                        case 2:
                                            bookBloodTest();
                                            break;
                                        case 3:
                                            surgeryBooking();
                                            break;
                                        case 4:
                                            visitBooking();
                                            break;
                                        case 5:
                                            //TODO: implement View Patient Folder Info
                                            break;
                                        case 6:
                                            setCatalogueCode();
                                            break;
                                        case 7:
                                            addMedicine();
                                            break;
                                        case 8:
                                            viewAllMedicines();
                                            break;
                                        case 9:
                                            createNewTherapy();
                                            break;
                                        case 10:
                                            break menuloop;
                                        default:
                                            break;
                                    }
                                    break;
                                case Surgeon:
                                    System.out.println("Select an option:");
                                    System.out.println("1. Select Surgery Team");
                                    choice = new Integer(scan.nextLine());
                                    switch (choice){
                                        case 1:
                                            //TODO: implement Select Surgery Team
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                            }
                        }
                    }

                    break;
                case 2:
                    System.out.println("Insert username:");
                    username = scan.nextLine();
                    System.out.println("Insert password:");
                    password = scan.nextLine();
                    System.out.println("Select role:");
                    System.out.println("0. Administrative Officer\n1. Receptionist\n2. Doctor\n3. Surgeon");

                    role = UserRole.values()[new Integer(scan.nextLine())];

                    boolean isRegistered = false;
                    try {
                        isRegistered = Controller.registerUser(username, password, role);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        continue;
                        //e.printStackTrace();
                    }

                    if (isRegistered) {
                        System.out.println(String.format("Registered as %s in role %s", username, role));
                        System.out.println("Now login to start using the system");
                    }
                    else {
                        System.out.println("DB issue occured. Please try again");
                    }

                    break;
                case 3:
                    System.out.println("Do you want to initialize the database? (Y/N)");
                    if(scan.nextLine().toLowerCase().equals("y") ){
                        DatabaseManager.resetDatabase();
                        DatabaseManager.initializeDatabase();
                    }
                    break;
                default:
                    choice = -1;
                    break;
            }
            if(choice == -1)
                break;
        }
        scan.close();
    }
}
