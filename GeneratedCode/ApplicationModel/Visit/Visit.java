/**
 * @(#) Visit.java
 */

package ApplicationModel.Visit;

import ApplicationModel.Medic.Doctor;
import ApplicationModel.Patient.PatientFolder;
import ApplicationModel.Therapy.Therapy;
import ApplicationModel.Tests.Test;
import ApplicationModel.Surgery.Surgery;

public class Visit
{
	private PatientFolder folder;
	
	private Doctor follower;
	
	private Doctor host;
	
	private String report;
	
	private VisitTimeSlot slot;
	
	private Therapy prescribedTherapies;
	
	private Surgery prescribedSurgeries;
	
	private Test prescribedTests;
	
	private String visitId;
	
	
}
