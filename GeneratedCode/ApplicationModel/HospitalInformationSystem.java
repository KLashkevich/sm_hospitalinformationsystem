/**
 * @(#) HospitalInformationSystem.java
 */

package ApplicationModel;

import ApplicationModel.Medic.Medic;
import ApplicationModel.Therapy.TherapyCalendar;
import ApplicationModel.Tests.BloodTestsCalendar;
import ApplicationModel.Surgery.SurgeryCalendar;
import ApplicationModel.Tests.ImagingTestsCalendar;
import ApplicationModel.Medicine.Medicine;
import ApplicationModel.Patient.Patient;
import ApplicationModel.Visit.VisitCalendar;

public class HospitalInformationSystem
{
	private Medic medics;
	
	private Patient patients;
	
	private Medicine medicines;
	
	private BloodTestsCalendar bloodCalendar;
	
	private VisitCalendar visitCalendar;
	
	private TherapyCalendar therapyCalendar;
	
	private SurgeryCalendar surgeryCalendar;
	
	private ImagingTestsCalendar imagingCalendar;
	
	
}
