/**
 * @(#) Controller.java
 */

package InteractionModel.HospitalInformationSystem;

public class Controller
{
	public void createPatientFolder( String name, String surname, String idCode, date birthDate, String insuranceType, String insCode, String insCompany )
	{
		
	}
	
	public void registerUser( String username, String password, UserRole role )
	{
		
	}
	
	public void loginUser( String username, String password )
	{
		
	}
	
	public void addOncologist( String name, String surname, OncologistType type, String careerLevel, String professionalID )
	{
		
	}
	
	public void getOncologists( )
	{
		
	}
	
	public void addSurgeon( String name, String surname, SurgeonType type, String professionalID )
	{
		
	}
	
	public void getSurgeons( )
	{
		
	}
	
	public void deletePatientFolder( String idCode )
	{
		
	}
	
	public void assignOncologist( String patientID, String professionalID )
	{
		
	}
	
	public void setFirstVisitDate( String idCode, date visitDate )
	{
		
	}
	
	public void getPatients( )
	{
		
	}
	
	public void setCatalogueCode( String patientId, String code )
	{
		
	}
	
	public void getAvailableDoctors( date visitDate )
	{
		
	}
	
	public void getDateSlots( date date, String professionalID )
	{
		
	}
	
	public void bookVisitDateAndTime( String timeSlotId, String patientId, String followerProfessionalID )
	{
		
	}
	
	public void getPatientVisits( String idCode )
	{
		
	}
	
	public void updateVisitReport( String visitId, String report )
	{
		
	}
	
	public void getBlTestsDaySlots( date date )
	{
		
	}
	
	public void bookBlTestDateTime( String patientId, String timeSlotId )
	{
		
	}
	
	public void getImgTestsDaySlots( date date )
	{
		
	}
	
	public void bookImgTestDateTime( String patientId, String timeSlotId )
	{
		
	}
	
	public void getPatientTests( String idCode )
	{
		
	}
	
	public String getTestReport( String testId )
	{
		return null;
	}
	
	public void getAvailableSurgeons( SurgeonType surgeonType, date date )
	{
		
	}
	
	public void bookSurgeryDate( date date, String professionalID, String patientId )
	{
		
	}
	
	public void addMedicine( String code, String name, String pharmaceuticalCompany )
	{
		
	}
	
	public void getMedicines( )
	{
		
	}
	
	public boolean checkTherapyOverlapping( String patientId, date startDate, date endDate )
	{
		return false;
	}
	
	public void addTreatmentMedicine( String medCode, date startDate, date endDate )
	{
		
	}
	
	public void viewPatientFolder( String idCode )
	{
		
	}
	
	public void getUpcomingSurgeries( String professionalID )
	{
		
	}
	
	public void getAvailableTeamMembers( String surgeryId )
	{
		
	}
	
	public void addTeamMember( String surgeryId, String professionalID )
	{
		
	}
	
	
}
