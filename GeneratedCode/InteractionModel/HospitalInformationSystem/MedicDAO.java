/**
 * @(#) MedicDAO.java
 */

package InteractionModel.HospitalInformationSystem;

import ApplicationModel.Medic.Surgeon;
import ApplicationModel.Medic.Oncologist;

public class MedicDAO
{
	public boolean medicExists( String professionalID )
	{
		return false;
	}
	
	public void insertOncologist( Oncologist oncologist )
	{
		
	}
	
	public void getOncologists( )
	{
		
	}
	
	public void insertSurgeon( Surgeon surgeon )
	{
		
	}
	
	public void getSurgeons( )
	{
		
	}
	
	public void getAvailableDoctors( date visitDate )
	{
		
	}
	
	public void getAvailableSurgeons( SurgeonType surgeonType, date date )
	{
		
	}
	
	public void createNewSurgery( date date, String professionalID, String patientId )
	{
		
	}
	
	public void getAvailableTeamMembers( String patientId )
	{
		
	}
	
	public boolean teamMemberExists( String surgeryId, String professionalID )
	{
		return false;
	}
	
	
}
