/**
 * @(#) PatientDAO.java
 */

package InteractionModel.HospitalInformationSystem;

import ApplicationModel.Patient.PatientFolder;

public class PatientDAO
{
	public boolean patientExists( String idCode )
	{
		return false;
	}
	
	public void deletePatientFolder( String idCode )
	{
		
	}
	
	public void insertPatientFolder( PatientFolder folder )
	{
		
	}
	
	public void updateOncologist( String patientID, String professionalID )
	{
		
	}
	
	public void updateFirstVisit( String idCode, date visitDate )
	{
		
	}
	
	public void getPatients( )
	{
		
	}
	
	public void updateCatalogueCode( String patientId, String code )
	{
		
	}
	
	public void getPatientFolderInfo( String idCode )
	{
		
	}
	
	
}
