In order to run the project you need to install and configure HSQL:
https://www.tutorialspoint.com/hsqldb/hsqldb_installation.htm?fbclid=IwAR0EMBnvQat4anUHD6vppR1-nMuUJVZbohpnUhwgW9Sx4u_72vFBLZ71F48

Before running HSQL server, add server.properties file to the root HSQL server folder.
To run HSQL server execute the following command in the root HSQL folder:
	java -classpath lib/hsqldb.jar org.hsqldb.server.Server

or you can run runserver.bat file to make it faster

If you want to test HSQL server and run queries there you may start ..\hsqldb\bin\runManagerSwing.bat
To connect to the database use the following address: jdbc:hsqldb:hsql://localhost/hospitaldb

In project settings be sure you use SDK level 7+



